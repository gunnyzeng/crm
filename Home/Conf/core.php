<?php 
 return array(
	'UPLOAD_CAPACITY'=>'30000',
	'UPLOAD_USER_CAPACITY'=>'1000',
	'UPLOAD_FILE_PIC_SIZE'=>'102400',
	'UPLOAD_FILE_PIC_TYPE'=>'jpg|gif|png|bmp',
	'UPLOAD_FILE_FILE_SIZE'=>'10000000000',
	'UPLOAD_FILE_FILE_TYPE'=>'rar|zip|txt|doc|docx|ppt|pptx|xls|xlsx',

);
?>