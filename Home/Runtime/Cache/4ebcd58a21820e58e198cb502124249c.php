<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加新客户|<?php echo ($configcache['Title']); ?></title>
<link rel="stylesheet" type="text/css" href="__CSS__/content.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/public.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/uploadify.css"  />
<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/Public.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
<script type="text/javascript" src="__JS__/check.js"></script>
<script type="text/javascript" src="__JS__/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="__JS__/jquery.uploadify.js"></script>
<script>
$(document).ready(function() {
    
    $('.submit').click(function() {
        var 
            Username = $("input[name='Customername']").val();
            UserID = $("input[name='UserID']").val();
            Email = $("input[name='Email']").val();
            Phone = $("input[name='Phone']").val();
            OfficePhone = $("input[name='OfficePhone']").val();
            ByPhone = $("input[name='OfficePhone']").val();
            Qq = $("input[name='Qq']").val();
            
        
        if (!tcheck(Username,'','请填写姓名')) { return false; }
        if (!tcheck(Email,'email','请输入正确的邮箱')) { return false; }
        if (!tcheck(Qq,'number','QQ号码必须是数字')) { return false; }
        if (!tcheck(Phone,'mobile','请输入正确的手机号')) { return false; }
        if (!tcheck(OfficePhone,'mobile','请输入正确的办公电话')) { return false; }
        if (!tcheck(ByPhone,'mobile','请输入正确的备用电话')) { return false; }
        wintq('正在处理，请稍后...',4,20000,0,'');
        $('form').submit();
    });
});
</script>







<script type="text/javascript">
<?php $timestamp = time();?>

   $(document).ready(function() { 
        $('#file_upload').uploadify({
        'formData'     : {
            'timestamp'     : '<?php echo $timestamp;?>',
            'token'         : '<?php echo md5($timestamp);?>',
            'dirName'       : 'Picture/',                               //图片目录
            'Uid'           : "<?php echo $_SESSION['ThinkUser']['ID'];?>",
            'sid'           : '<?php echo ($sid); ?>',
        },
        'swf'           :   '__IMAGE__/uploadify.swf',                  //swf文件（必选）
        'uploader'      :   '__APP__/Upload/picdo/',                    //服务端脚本（必选）
        'method'        :   'post',
        //'buttonCursor'  : 'hand',                                     //设置的光标悬停在浏览按钮时显示。可能的值是'hand(手)'和'arrow(箭头)'
        'buttonText'    :   '选择图片',
        //'debug'           :   true,                                   //设置为true来打开SWFUpload的调试模式
        //'fileObjName' :   'fileName',                                 //设置后台接收的值$_FILES['file_name']
        'fileSizeLimit' :   '<?php echo ($filesize); ?>',                                //允许的最大尺寸文件上传，默认单位是KB，0则没有限制
        'fileTypeDesc'  :   'Image Type',                               //可选择的文件的描述。该字符串出现在浏览文件对话框的文件类型下拉
        'fileTypeExts'  :   '<?php echo ($filetype); ?>',                    //设置允许上传的文件类型
        'width'         :   100,                                        //浏览器按钮的宽度
        'height'        :   30,                                         //浏览器按钮的高度。
        'multi'         :   true,                                       //是否允许选择多个文件
        'auto'          :   true,                                      //选择图片后是否自动上传
        'preventCaching':   false,                                      //是否缓存
        'progressData'  :   'percentage',                               //数据文件上传进度更新时队列中的项目显示，这两个选项是“percentage(百分比)”或“speed(速度)”。
        //'queueID'     :   'queue',                                    //显示上传文件队列的元素id，可以简单用一个div来显示
        'queueSizeLimit':   1,                                         //队列允许文件的最大数量
        'uploadLimit'   :   900,                                        //允许的最大文件数量。当达到或超过此值时，onUploadError事件被触发
        //'cancelImage' :   'uploadify-cancel.png',                     //取消上传图片
        'removeCompleted':  true,                                       //上传成功后的文件，是否在队列中自动删除
        'removeTimeout' :   1,                                          //几秒后删除
        'requeueErrors' :   false,                                      //如果设置为true，即在上传过程中返回错误文件被重新排队和上传反复尝试。
        'successTimeout':   30,                                         //几秒钟的时间来等待服务器的响应
        
                                                                    //引发文件上传的进度每次更新
        'onUploadSuccess' : function(file,data,response) {              //触发每个成功上传文件
           var res = eval(data);
            if (res.length>0) {
                path = res[0].savepath+res[0].savename;
               $('#userthumb').attr("src","__ROOT__/"+path);
               $('#userthumbpath').val(path);
               $('#userclear').css("display","");
               $('#userview').attr("href","__ROOT__/"+path);
               $('#userview').css("display","");
            }
        },
    });
   
});


   $(document).ready(function() { 
        $('#xueli_upload').uploadify({
        'formData'     : {
            'timestamp'     : '<?php echo $timestamp;?>',
            'token'         : '<?php echo md5($timestamp);?>',
            'dirName'       : 'Picture/',                               //图片目录
            'Uid'           : "<?php echo $_SESSION['ThinkUser']['ID'];?>",
            'sid'           : '<?php echo ($sid); ?>',
        },
        'swf'           :   '__IMAGE__/uploadify.swf',                  //swf文件（必选）
        'uploader'      :   '__APP__/Upload/picdo/',                    //服务端脚本（必选）
        'method'        :   'post',
        //'buttonCursor'  : 'hand',                                     //设置的光标悬停在浏览按钮时显示。可能的值是'hand(手)'和'arrow(箭头)'
        'buttonText'    :   '选择图片',
        //'debug'           :   true,                                   //设置为true来打开SWFUpload的调试模式
        //'fileObjName' :   'fileName',                                 //设置后台接收的值$_FILES['file_name']
        'fileSizeLimit' :   '<?php echo ($filesize); ?>',                                //允许的最大尺寸文件上传，默认单位是KB，0则没有限制
        'fileTypeDesc'  :   'Image Type',                               //可选择的文件的描述。该字符串出现在浏览文件对话框的文件类型下拉
        'fileTypeExts'  :   '<?php echo ($filetype); ?>',                    //设置允许上传的文件类型
        'width'         :   100,                                        //浏览器按钮的宽度
        'height'        :   30,                                         //浏览器按钮的高度。
        'multi'         :   true,                                       //是否允许选择多个文件
        'auto'          :   true,                                      //选择图片后是否自动上传
        'preventCaching':   false,                                      //是否缓存
        'progressData'  :   'percentage',                               //数据文件上传进度更新时队列中的项目显示，这两个选项是“percentage(百分比)”或“speed(速度)”。
        //'queueID'     :   'queue',                                    //显示上传文件队列的元素id，可以简单用一个div来显示
        'queueSizeLimit':   1,                                         //队列允许文件的最大数量
        'uploadLimit'   :   900,                                        //允许的最大文件数量。当达到或超过此值时，onUploadError事件被触发
        //'cancelImage' :   'uploadify-cancel.png',                     //取消上传图片
        'removeCompleted':  true,                                       //上传成功后的文件，是否在队列中自动删除
        'removeTimeout' :   1,                                          //几秒后删除
        'requeueErrors' :   false,                                      //如果设置为true，即在上传过程中返回错误文件被重新排队和上传反复尝试。
        'successTimeout':   30,                                         //几秒钟的时间来等待服务器的响应
        
                                                                    //引发文件上传的进度每次更新
        'onUploadSuccess' : function(file,data,response) {              //触发每个成功上传文件
           var res = eval(data);
            if (res.length>0) {
                path = res[0].savepath+res[0].savename;
               $('#xuelithumb').attr("src","__ROOT__/"+path);
               $('#xuelithumbpath').val(path);
               $('#xueliclear').css("display","");
               $('#xueliview').attr("href","__ROOT__/"+path);
               $('#xueliview').css("display","");
            }
        },
    });
   
});


   $(document).ready(function() { 
        $('#shenfenf_upload').uploadify({
        'formData'     : {
            'timestamp'     : '<?php echo $timestamp;?>',
            'token'         : '<?php echo md5($timestamp);?>',
            'dirName'       : 'Picture/',                               //图片目录
            'Uid'           : "<?php echo $_SESSION['ThinkUser']['ID'];?>",
            'sid'           : '<?php echo ($sid); ?>',
        },
        'swf'           :   '__IMAGE__/uploadify.swf',                  //swf文件（必选）
        'uploader'      :   '__APP__/Upload/picdo/',                    //服务端脚本（必选）
        'method'        :   'post',
        //'buttonCursor'  : 'hand',                                     //设置的光标悬停在浏览按钮时显示。可能的值是'hand(手)'和'arrow(箭头)'
        'buttonText'    :   '选择图片',
        //'debug'           :   true,                                   //设置为true来打开SWFUpload的调试模式
        //'fileObjName' :   'fileName',                                 //设置后台接收的值$_FILES['file_name']
        'fileSizeLimit' :   '<?php echo ($filesize); ?>',                                //允许的最大尺寸文件上传，默认单位是KB，0则没有限制
        'fileTypeDesc'  :   'Image Type',                               //可选择的文件的描述。该字符串出现在浏览文件对话框的文件类型下拉
        'fileTypeExts'  :   '<?php echo ($filetype); ?>',                    //设置允许上传的文件类型
        'width'         :   100,                                        //浏览器按钮的宽度
        'height'        :   30,                                         //浏览器按钮的高度。
        'multi'         :   true,                                       //是否允许选择多个文件
        'auto'          :   true,                                      //选择图片后是否自动上传
        'preventCaching':   false,                                      //是否缓存
        'progressData'  :   'percentage',                               //数据文件上传进度更新时队列中的项目显示，这两个选项是“percentage(百分比)”或“speed(速度)”。
        //'queueID'     :   'queue',                                    //显示上传文件队列的元素id，可以简单用一个div来显示
        'queueSizeLimit':   1,                                         //队列允许文件的最大数量
        'uploadLimit'   :   900,                                        //允许的最大文件数量。当达到或超过此值时，onUploadError事件被触发
        //'cancelImage' :   'uploadify-cancel.png',                     //取消上传图片
        'removeCompleted':  true,                                       //上传成功后的文件，是否在队列中自动删除
        'removeTimeout' :   1,                                          //几秒后删除
        'requeueErrors' :   false,                                      //如果设置为true，即在上传过程中返回错误文件被重新排队和上传反复尝试。
        'successTimeout':   30,                                         //几秒钟的时间来等待服务器的响应
        
                                                                    //引发文件上传的进度每次更新
        'onUploadSuccess' : function(file,data,response) {              //触发每个成功上传文件
           var res = eval(data);
            if (res.length>0) {
                path = res[0].savepath+res[0].savename;
               $('#shenfenfthumb').attr("src","__ROOT__/"+path);
               $('#shenfenfthumbpath').val(path);
               $('#shenfenfclear').css("display","");
               $('#shenfenfview').attr("href","__ROOT__/"+path);
               $('#shenfenfview').css("display","");
            }
        },
    });
   
});

  $(document).ready(function() { 
    $('#shenfenz_upload').uploadify({
    'formData'     : {
        'timestamp'     : '<?php echo $timestamp;?>',
        'token'         : '<?php echo md5($timestamp);?>',
        'dirName'       : 'Picture/',                               //图片目录
        'Uid'           : "<?php echo $_SESSION['ThinkUser']['ID'];?>",
        'sid'           : '<?php echo ($sid); ?>',
    },
    'swf'           :   '__IMAGE__/uploadify.swf',                  //swf文件（必选）
    'uploader'      :   '__APP__/Upload/picdo/',                    //服务端脚本（必选）
    'method'        :   'post',
    //'buttonCursor'  : 'hand',                                     //设置的光标悬停在浏览按钮时显示。可能的值是'hand(手)'和'arrow(箭头)'
    'buttonText'    :   '选择图片',
    //'debug'           :   true,                                   //设置为true来打开SWFUpload的调试模式
    //'fileObjName' :   'fileName',                                 //设置后台接收的值$_FILES['file_name']
    'fileSizeLimit' :   '<?php echo ($filesize); ?>',                                //允许的最大尺寸文件上传，默认单位是KB，0则没有限制
    'fileTypeDesc'  :   'Image Type',                               //可选择的文件的描述。该字符串出现在浏览文件对话框的文件类型下拉
    'fileTypeExts'  :   '<?php echo ($filetype); ?>',                    //设置允许上传的文件类型
    'width'         :   100,                                        //浏览器按钮的宽度
    'height'        :   30,                                         //浏览器按钮的高度。
    'multi'         :   true,                                       //是否允许选择多个文件
    'auto'          :   true,                                      //选择图片后是否自动上传
    'preventCaching':   false,                                      //是否缓存
    'progressData'  :   'percentage',                               //数据文件上传进度更新时队列中的项目显示，这两个选项是“percentage(百分比)”或“speed(速度)”。
    //'queueID'     :   'queue',                                    //显示上传文件队列的元素id，可以简单用一个div来显示
    'queueSizeLimit':   1,                                         //队列允许文件的最大数量
    'uploadLimit'   :   900,                                        //允许的最大文件数量。当达到或超过此值时，onUploadError事件被触发
    //'cancelImage' :   'uploadify-cancel.png',                     //取消上传图片
    'removeCompleted':  true,                                       //上传成功后的文件，是否在队列中自动删除
    'removeTimeout' :   1,                                          //几秒后删除
    'requeueErrors' :   false,                                      //如果设置为true，即在上传过程中返回错误文件被重新排队和上传反复尝试。
    'successTimeout':   30,                                         //几秒钟的时间来等待服务器的响应
    
                                                                //引发文件上传的进度每次更新
    'onUploadSuccess' : function(file,data,response) {              //触发每个成功上传文件
       var res = eval(data);
        if (res.length>0) {
            path = res[0].savepath+res[0].savename;
           $('#shenfenzthumb').attr("src","__ROOT__/"+path);
           $('#shenfenzthumbpath').val(path);
           $('#shenfenzclear').css("display","");
           $('#shenfenzview').attr("href","__ROOT__/"+path);
           $('#shenfenzview').css("display","");
        }
    },
});

});


  function extractionBirthday()
{

        var userid = $("input[name='UserID']").val();

        if(userid.length==18)

        {
            //设置出生年月
        　　var year = userid.substring(6,10);

        　　var month = userid.substring(10,12);

        　　var date=userid.substring(12,14);

        　　$("input[name='Birthday']").val(year+"-"+month+"-"+date);
            //设置性别
            var sex = parseInt(""+userid.charAt(16))+1;
             $("#Sex").find("option").eq(sex%2).attr("selected","selected");
           
        }
        else
        {
            alert("身份证不正确！");
        }

}
function  dateDiff(d1,str){
        d1 = new Date(d1.replace(/-/g,'/'));
        d2 = new Date();
        var obj={},M1=d1.getMonth(),D1=d1.getDate(),M2=d2.getMonth(),D2=d2.getDate();
        obj.Y=d2.getFullYear() - d1.getFullYear() + (M1*100+D1 > M2*100+D2 ? -1 : 0);
        obj.M=obj.Y * 12 + M2 - M1 + (D1 > D2 ? -1 : 0);
        obj.s=Math.floor((d2-d1)/1000);//差几秒
        obj.m=Math.floor(obj.s/60);//差几分钟
        obj.h=Math.floor(obj.m/60);//差几小时
        obj.D=Math.floor(obj.h/24);//差几天  
       
        return obj.Y;
        
}
function setJobYears(){
    var date1 = $("input[name='StartJobDate']").val();
    if(date1==''){
        return;
    }
    var d1 = new Date(date1.replace(/-/g,'/'));
    var d2 = new Date();
    if(d2<d1){
        $("input[name='StartJobDate']").focus();
        alert("参加工作时间有误");
      
    }
     $("input[name='JobYears']").val(dateDiff(date1));
}
function clearimg(cid){
    $('#'+cid+'thumb').removeAttr('src');
    $('#'+cid+'thumbpath').val('');
    $('#'+cid+'clear').css("display","none");
    $('#'+cid+'view').css("display","none");
}

</script>


</head>
<body>
<div id="content" style="margin:0 auto; width:97%;">

    <h3>
        <a href="javascript:;" onclick="location.reload();">刷新</a>
        <a href="__APP__/Client/clientedit?id=<?php echo ($result['ID']); ?>" class="h3a">客户资料</a>
        <a href="__APP__/Client/wincontact?Cid=<?php echo ($result['ID']); ?>">成交记录(<?php echo ($result['Salecount']); ?>)</a>
        <a href="__APP__/With/winwith?Cid=<?php echo ($result['ID']); ?>">提醒记录(<?php echo ($result['Tipcount']); ?>)</a>
    </h3>
    
   

    <form action="__APP__/Client/clientedit_do" method="post">
    <input type="hidden" value="<?php echo ($Uid); ?>" name="uid">
    <table id="client" style="border: none;float: left;" bordercolor="#CCCCCC" cellpadding="0" cellspacing="0">

        <tr class="tr">
            <td class="left">姓名：</td>
            <td><input name="Customername" type="text" class="ctext" size="20" value="<?php echo ($result['Customername']); ?>"/><font>* </font></td>
            <td class="left">手机：</td>
            <td><input name="Phone" type="text" class="ctext" size="20" value="<?php echo ($result['Phone']); ?>"/></td>
            <td class="left">客户级别：</td>
            <td>
                <select name="CustomerLevel" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 57): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['CustomerLevel'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
           
        </tr>
        <tr class="tr">
            <td class="left">身份证号：</td>
            <td><input name="UserID" type="text" class="ctext" size="20" onblur="extractionBirthday();" id="UserID" value="<?php echo ($result['UserID']); ?>" /></td>
            <td class="left">备用电话：</td>
            <td><input name="ByPhone" type="text" class="ctext" size="20" value="<?php echo ($result['ByPhone']); ?>" /></td>
            <td class="left">工作单位：</td>
             <td><input name="CompanyName" type="text" class="ctext" size="20" value="<?php echo ($result['CompanyName']); ?>" /></td>
           
        </tr>
        <tr class="tr">
            <td class="left">生日：</td>
            <td>
                <input class="Wdate" name="Birthday" type="text" value="<?php echo ($result['Birthday']); ?>" onfocus="WdatePicker({startDate:'%y-%M-01',dateFmt:'yyyy-MM-dd',alwaysUseStartDate:true})">
            </td>
            <td class="left">QQ：</td>
            <td><input name="Qq" type="text" class="ctext" size="20" value="<?php echo ($result['Qq']); ?>" /></td>
            <td class="left" style="font-size: 13px;">职位：</td>
            <td>
                 <input name="Post" type="text" class="ctext" size="20" value="<?php echo ($result['Post']); ?>" />
            </td>
        </tr>
          <tr class="tr">
         <td class="left">性别：</td>
            <td>
                <select id="Sex" name="Sex" class="select">
                   <option value="男" <?php if($result['Sex'] == '男'): ?>selected<?php endif; ?>>男</option>
                    <option value="女" <?php if($result['Sex'] == '女'): ?>selected<?php endif; ?>>女</option>
                </select>
            </td>
            
             <td class="left">Email：</td>
            <td><input name="Email" type="text" class="ctext" size="20" value="<?php echo ($result['Email']); ?>"/></td>
             <td class="left" >培训经历：</td>
            <td><select name="Peixun" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 68): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['Peixun'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
             </td>
        </tr>
        <tr class="tr">
             <td class="left">最高学历：</td>
            <td>
                <select name="Education" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 107): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['Education'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
            <td class="left">办公电话：</td>
            <td><input name="OfficePhone" type="text" class="ctext" size="20" value="<?php echo ($result['OfficePhone']); ?>" /></td>
            <td class="left">毕业院校：</td>
            <td><input name="School" type="text" class="ctext" size="20" value="<?php echo ($result['School']); ?>" /></td>
        </tr>
        <tr class="tr">
            <td class="left" width="120px" style="font-size: 13px">参加工作时间：</td>
             <td><input class="Wdate" name="StartJobDate" type="text" value="<?php echo ($result['StartJobDate']); ?>" onfocus="WdatePicker({startDate:'%y',dateFmt:'yyyy',alwaysUseStartDate:true})" onblur="setJobYears()"></td>
             <td class="left">单位地址：</td>
             <td><input name="CompanyAddress" type="text" class="ctext" size="20" value="<?php echo ($result['CompanyAddress']); ?>" /></td>
              <td class="left">所学专业：</td>
            <td><input name="Major" type="text" class="ctext" size="20"  value="<?php echo ($result['Major']); ?>"/></td>
        </tr>
        <tr class="tr">
         <td class="left">工作年限：</td>
            <td><input name="JobYears" type="text" class="ctext" size="20" value="<?php echo ($result['JobYears']); ?>"/></td>
            <td class="left">家庭地址：</td>
            <td><input name="Address" type="text" class="ctext" size="20" value="<?php echo ($result['Address']); ?>"/></td>
             <td class="left">兴趣爱好：</td>
             <td><input name="Hobbies" type="text" class="ctext" size="20" value="<?php echo ($result['Hobbies']); ?>"/></td>
        </tr>
        <tr class="tr">
           <td class="left">介绍人：</td>
           <td><input name="Intro" type="text" class="ctext" size="20" value="<?php echo ($result['Intro']); ?>" /></td>
           <td class="left">来源：</td>
            <td>
              <?php if($Uid == ''): ?><select name="From" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 2): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['From'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
                <?php else: ?>
                    <input name="From" type="text" class="ctext" size="20" value="客户填写" readonly="true" /><?php endif; ?>
            </td>
          
            <td class="left">是否共享：</td>
          <td>
            <select name="OpenShare" class="select">
                <option value="0" <?php if($result['OpenShare'] == '0' ): ?>selected<?php endif; ?>>否</option>
                <option value="1" <?php if($result['OpenShare'] == '1' ): ?>selected<?php endif; ?>>是</option>
            </select>
          </td>
            
        </tr>
          <tr class="tr">
            <td class="left">备注说明：</td>
            <td colspan="3"><textarea name="Content" class="textarea" style="width:400px; height:60px; margin:6px 0px;"><?php echo ($result['Content']); ?></textarea></td>
             <td> 个人照片：<input id="file_upload"  type="file" multiple="true"></td>
            <td> 
                <img id="userthumb" width="100px;" <?php if($result['Userthumbpath'] != ''): ?>src="__ROOT__/<?php echo ($result['Userthumbpath']); ?>"<?php endif; ?>></img>
                <a <?php if($result['Userthumbpath'] != ''): ?>href="__ROOT__/<?php echo ($result['Userthumbpath']); ?>"<?php endif; ?> id="userview" target="_blank" title="点击预览" <?php if($result['Userthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>(点击预览)</a>
                <img id="userclear" src="__IMAGE__/no.png" onclick="clearimg('user')" title="点击删除"  class="openshare" border="0" <?php if($result['Userthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>
                <input type="hidden" id="userthumbpath" name="Userthumbpath" value="<?php echo ($result['Userthumbpath']); ?>">
            </td>
         </tr>

        <tr class="tr">
            <td>学历证：  <input id="xueli_upload"  type="file" multiple="true"> </td>
            <td> 
            <img id="xuelithumb" width="100px;"  <?php if($result['Xuelithumbpath'] != ''): ?>src="__ROOT__/<?php echo ($result['Xuelithumbpath']); ?>"<?php endif; ?>></img> 
             <a <?php if($result['Xuelithumbpath'] != ''): ?>href="__ROOT__/<?php echo ($result['Xuelithumbpath']); ?>"<?php endif; ?> id="xueliview" target="_blank" title="点击预览" <?php if($result['Userthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>(点击预览)</a>
            <img id="xueliclear" src="__IMAGE__/no.png" onclick="clearimg('xueli')" title="点击删除"  class="openshare" border="0" <?php if($result['Xuelithumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>
            <input type="hidden" id="xuelithumbpath" name="Xuelithumbpath" value="<?php echo ($result['Xuelithumbpath']); ?>">
            </td>
            <td>身份证(正)：<input id="shenfenz_upload"  type="file" multiple="true"></td>
            <td> 
            
            <img id="shenfenzthumb" width="100px;"  <?php if($result['Shenfenzthumbpath'] != ''): ?>src="__ROOT__/<?php echo ($result['Shenfenzthumbpath']); ?>"<?php endif; ?>></img>
            <a <?php if($result['Shenfenzthumbpath'] != ''): ?>href="__ROOT__/<?php echo ($result['Shenfenzthumbpath']); ?>"<?php endif; ?> id="shenfenzview" target="_blank" title="点击预览" <?php if($result['Userthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>(点击预览)</a>
          <img id="shenfenzclear" src="__IMAGE__/no.png" onclick="clearimg('shenfenz')" title="点击删除"  class="openshare" border="0" <?php if($result['Shenfenzthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>
            <input type="hidden" id="shenfenzthumbpath" name="Shenfenzthumbpath" value="<?php echo ($result['Shenfenzthumbpath']); ?>">
            </td>
            <td>身份证(反)：  <input id="shenfenf_upload"  type="file" multiple="true"> </td>
            <td> 
            <img id="shenfenfthumb" width="100px;" <?php if($result['Shenfenfthumbpath'] != ''): ?>src="__ROOT__/<?php echo ($result['Shenfenfthumbpath']); ?>"<?php endif; ?>></img>
            <a <?php if($result['Shenfenfthumbpath'] != ''): ?>href="__ROOT__/<?php echo ($result['Shenfenfthumbpath']); ?>"<?php endif; ?> id="shenfenfview" target="_blank" title="点击预览" <?php if($result['Userthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>(点击预览)</a>
            <img id="shenfenfclear" src="__IMAGE__/no.png" onclick="clearimg('shenfenf')" title="点击删除"  class="openshare" border="0" <?php if($result['Shenfenfthumbpath'] == ''): ?>style="display:none;"<?php endif; ?>>
            <input type="hidden" id="shenfenfthumbpath" name="Shenfenfthumbpath" value="<?php echo ($result['Shenfenfthumbpath']); ?>">
            </td>
                
        </tr>
     
        <tr>

        <td class="left">录入日期：</td>
        <td><input name="Dtime" type="text" class="ctext" size="20" readonly="true"  value="<?php echo ($result['Dtime']); ?>" /></td>
        <td class="left">录入人：</td>
        <td><input name="Username" type="text" class="ctext" size="20" readonly="true"  value="<?php echo ($result['Username']); ?>" /></td>
        <td colspan="4" style="border: solid 0px;"> 
        <input type="hidden" name="ID" value="<?php echo ($result['ID']); ?>" />
        <input type="submit" class="submit" value="提交" style="margin:20px 0 0 0px;float: right;" />
        </td>

        </tr>
    </table>
   
    </form>
</div>
</body>
</html>