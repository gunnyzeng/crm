<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo ($configcache['Title']); ?></title>
<link rel="stylesheet" type="text/css" href="__CSS__/public.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/content.css"  />
<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
</head>
<body>
<div id="content">
	<div id="con1">
    	<h6>系统信息</h6>
        <ul>
            <li><span>云端总容量：</span><font><?php echo ($info['core']['UPLOAD_CAPACITY']); ?> M</font></li>
            <li><span>用户空间分配：</span><font><?php echo ($info['core']['UPLOAD_USER_CAPACITY']); ?> M</font></li>
            <li><span>图片大小限制：</span><font><?php echo ($info['core']['UPLOAD_FILE_PIC_SIZE']); ?> KB</font></li>
            <li><span>图片类型限制：</span><?php echo ($info['core']['UPLOAD_FILE_PIC_TYPE']); ?></li>
            <li><span>文件大小限制：</span><font><?php echo ($info['core']['UPLOAD_FILE_FILE_SIZE']); ?> KB</font></li>
            <li><span>文件类型限制：</span><?php echo ($info['core']['UPLOAD_FILE_FILE_TYPE']); ?></li>
            <li><span>在线人数：</span><font><?php echo ($usercount); ?> 人</font></li>
        </ul>
    </div>
	<div id="con2">
    	<h6>基本信息</h6>
        <ul>
        	<dt><strong>客户信息</strong></dt>
        	<li><span>客户总数：</span><strong><?php echo ($info['clientcount']); ?></strong> 位</li>
            <li><span>成交客户总数：</span><strong><?php echo ($info['clientSaled']); ?></strong> 位</li>
            <li><span>未成交客户总数：</span><strong><?php echo ($info['clientNoSaled']); ?></strong> 位</li>
            <li><span>成交记录：</span><strong><?php echo ($info['contactcount']); ?></strong> 位</li>
            <li><span>成交金额：</span><strong><?php echo ($info['Prices']); ?></strong> 元</li>
            <dt><strong>其它信息</strong></dt>
            <li><span>用户总数：</span><strong><?php echo ($info['user']); ?></strong> 位</li>
            <li><span>云端文件总数：</span><strong><?php echo ($info['file']); ?></strong> 个</li>
		</ul>
    </div>
 
</div>
</body>
</html>