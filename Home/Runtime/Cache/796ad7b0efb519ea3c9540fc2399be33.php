<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>成交记录|<?php echo ($configcache['Title']); ?></title>
<link rel="stylesheet" type="text/css" href="__CSS__/content.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/public.css"  />
<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/Public.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
<script>
$(document).ready(function() {
	function Operating() {
		//新增
		$('#content h3 .add').click(function(event) {
			event.preventDefault();
			var url = $(this).attr('href');
			window.top.c.window.wincontactAdd(url);
		});
		//修改
		$('#content #table .tr .edit').click(function(event) {
			event.preventDefault();
			var id=$(this).attr('href');
			if (id=='' || isNaN(id)) {
				wintq('ID参数不正确',3,1000,1,'');
				return false;
			}else {
				window.location.href  = '__APP__/Client/contactedit/id/'+id;
				// popload('修改联系人信息',860,430,);
				// addDiv($('#iframe_pop'));
				// popclose();
			}
		});
		$('#content #table .tr .del').click(function(event) {
			event.preventDefault();
			if (!confirm('确定要删除该数据吗？')) {
				return false;
			}
			var id=$(this).attr('href');
			if (id=='' || isNaN(id)) {
				wintq('ID参数不正确',3,1000,1,'');
				return false;
			}else {
				wintq('正在删除，请稍后...',4,20000,0,'');
				$.ajax({
					url:'__APP__/Client/contact_del/',
					dataType:'json',
					type:'POST',
					data:'post=ok&id='+id,
					success: function(data) {
						if (data.s=='ok') {
							wintq('删除成功',1,1500,0,'?');
						}else {
							wintq(data.s,3,1500,1,'');
						}
					}
				});
			}
		});
		$('#dely').click(function(event) {
			event.preventDefault();
			if (!confirm('确定要删除选择项吗？')) {
				return false;
			}
			var delid='';
			for (i=0; i<$('#table .delid').size(); i++) {
				if (!$('#table .delid').eq(i).attr('checked')==false) {
					delid=delid+$('#table .delid').eq(i).val()+',';
				}
			}
			if (delid=='') {
				wintq('请选中后再操作',2,1500,1,'');
			}else {
				wintq('正在删除，请稍后...',4,20000,0,'');
				$.ajax({
					url:'__APP__/Client/contact_indel/',
					dataType:'JSON',
					type:'POST',
					data:'delid='+delid,
					success: function(data) {
						if (data.s=='ok') {
							wintq('删除成功',1,1500,0,'?');
						}else {
							wintq(data.s,3,1500,1,'');
						}
					}
				});
			}
		});
		//分页
		$('#page .page a').click(function(event) {
			event.preventDefault();
			var url = $(this).attr('href');
			contactajax(url);
		});
	}
	//拉取客户信息
	function contactajax(url) {
		$.get(url, function(data) {
			//回调函数
			data = eval('('+data+')');
			if (data.s=='ok') {
				//有数据的情况下
				$('#table .tr').remove();
				$('#page .page').remove();
				$('#table').append(data.html);
				$('#page').append(data.page);
				$('#page').append("&nbsp;&nbsp;&nbsp;<font color='red'>金额："+data.Price+"</font>");
			}else {
				//没有数据的情况下
				$('#table .tr').remove();
				$('#page .page').remove();
				$('#table').append(data.html);
			}
			Operating();
		});
	}
	contactajax('__APP__/Client/contactajax?Cid=<?php echo ($Cid); ?>');
});
function exportExcel(){
	       var selectid='';
			for (i=0; i<$('#table .delid').size(); i++) {
				if (!$('#table .delid').eq(i).attr('checked')==false) {
					selectid=selectid+$('#table .delid').eq(i).val()+',';
				}
			}
	    var query =  "" ;
		$(".select").each(function(){
				query += $(this).val()+"||"
		})

        var value = $('.search .text').val();
		var querydate =  $("input[name='querydate']").val();
		if(selectid!=""){
			alert(selectid);
			window.location.href = "__APP__/Client/exportContacts?selectid="+selectid;
		}else{
			window.location.href = "__APP__/Client/exportContacts?Cid=<?php echo ($Cid); ?>&keyword="+value+"&query="+query+"&querydate="+querydate;
		}
		
}
</script>
</head>
<body>
<div id="content" style="margin:0 auto; width:97%;">
    <h3>
    	<a href="javascript:;" onclick="location.reload();">刷新</a>
    	<a href="__APP__/Client/clientedit?id=<?php echo ($Cid); ?>">客户信息</a>
    	<a href="__APP__/Client/wincontact?Cid=<?php echo ($Cid); ?>" class="h3a">成交记录</a>
       <!--  <a href="__APP__/Client/contactadd?Cid=<?php echo ($Cid); ?>" class="add">新增销售信息</a> -->
        <a href="__APP__/With/winwith?Cid=<?php echo ($Cid); ?>">提醒记录</a>
    </h3>
    <table id="table" border="1" bordercolor="#CCCCCC" cellpadding="0" cellspacing="0">
    	<tr>
        	<th><input type="checkbox" class="indel" value="del" /></th>
            <th>姓名</th>
            <th>身份证号</th>
		  	<th>学历</th>
            <th>考期</th>
            <th>状态</th>
            <th>项目</th>
            <th>价格</th>
            <th>支付方式</th>
            <th>成交时间</th>
            <th>操作</th>
        </tr>
    </table>
    <div id="page"><a href="javascript:;" class="selbox">全选</a><a href="javascript:;" class="anti">反选</a><a href="javascript:;" class="unselbox">全不选</a>&nbsp;&nbsp;对选中项进行&nbsp;&nbsp;<a href="javascript:;" id="dely">删除</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="exportExcel();" >导出</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="__APP__/Client/contactadd?Cid=<?php echo ($Cid); ?>" >新增</a></div>
</div>
</body>
</html>