<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>提醒记录|<?php echo ($configcache['Title']); ?></title>
<link rel="stylesheet" type="text/css" href="__CSS__/content.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/public.css"  />
<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/Public.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
<script>
//新增跟单记录
function withAdd(url) {
	fpopload('(关闭不刷新，需点击-提醒记录-才能显示最新添加^_^)',550,350,url);
	addDiv($('#f_pop'));
}
$(document).ready(function() {
	function Operating() {
		$('#content #table .tr .edit').click(function(event) {
			event.preventDefault();
			var id=$(this).attr('href');
			if (id=='' || isNaN(id)) {
				wintq('ID参数不正确',3,1000,1,'');
				return false;
			}else {
				popload('修改提示内容',550,350,'__APP__/With/withedit/id/'+id);
				addDiv($('#iframe_pop'));
				popclose();
			}
		});
		$('#content #table .tr .del').click(function(event) {
			event.preventDefault();
			if (!confirm('确定要删除该数据吗？')) {
				return false;
			}
			var id=$(this).attr('href');
			if (id=='' || isNaN(id)) {
				wintq('ID参数不正确',3,1000,1,'');
				return false;
			}else {
				wintq('正在删除，请稍后...',4,20000,0,'');
				$.ajax({
					url:'__APP__/With/with_del/',
					dataType:'json',
					type:'POST',
					data:'post=ok&id='+id,
					success: function(data) {
						if (data.s=='ok') {
							wintq('删除成功',1,1500,0,'?');
						}else {
							wintq(data.s,3,1500,1,'');
						}
					}
				});
			}
		});
		$('#dely').click(function(event) {
			event.preventDefault();
			if (!confirm('确定要删除选择项吗？')) {
				return false;
			}
			var delid='';
			for (i=0; i<$('#table .delid').size(); i++) {
				if (!$('#table .delid').eq(i).attr('checked')==false) {
					delid=delid+$('#table .delid').eq(i).val()+',';
				}
			}
			if (delid=='') {
				wintq('请选中后再操作',2,1500,1,'');
			}else {
				wintq('正在删除，请稍后...',4,20000,0,'');
				$.ajax({
					url:'__APP__/With/with_indel/',
					dataType:'JSON',
					type:'POST',
					data:'delid='+delid,
					success: function(data) {
						if (data.s=='ok') {
							wintq('删除成功',1,1500,0,'?');
						}else {
							wintq(data.s,3,1500,1,'');
						}
					}
				});
			}
		});
		$('#table .tr .compyname').click(function(event) {
			event.preventDefault();
			var url = $(this).attr('href');
			contactajax(url);
		});
		//分页
		$('#page .page a').click(function(event) {
			event.preventDefault();
			var url = $(this).attr('href');
			contactajax(url);
		});
	}
	//拉取客户信息
	function contactajax(url) {
		$.get(url, function(data) {
			//回调函数
			data = eval('('+data+')');
			if (data.s=='ok') {
				//有数据的情况下
				$('#table .tr').remove();
				$('#page .page').remove();
				$('#table').append(data.html);
				$('#page').append(data.page);
			}else {
				//没有数据的情况下
				$('#table .tr').remove();
				$('#page .page').remove();
				$('#table').append(data.html);
			}
			Operating();
		});
	}
	contactajax('__APP__/With/withajax?Cid=<?php echo ($Cid); ?>&keyword=');
	var speed='';
	$('.search .text').keyup(function() {
		clearTimeout(speed);
		var value = $(this).val();
		speed = setTimeout(function() {
			contactajax('__APP__/With/withajax?Cid=<?php echo ($Cid); ?>&keyword='+value);
		},300);
	});
	$('.so').click(function() {
		var value = $('.search .text').val();
		contactajax('__APP__/With/withajax?Cid=<?php echo ($Cid); ?>&keyword='+value);
		
	});
});
</script>
</head>
<body>
<div id="content" style="margin:0 auto; width:97%;">
    <h3>
    	<a href="javascript:;" onclick="location.reload();">刷新</a>
    	<a href="__APP__/Client/clientedit?id=<?php echo ($Cid); ?>">客户信息</a>
    	<a href="__APP__/Client/wincontact?Cid=<?php echo ($Cid); ?>" >成交记录</a>
       <!--  <a href="__APP__/Client/contactadd?Cid=<?php echo ($Cid); ?>" class="add">新增销售信息</a> -->
        <a href="__APP__/With/winwith?Cid=<?php echo ($Cid); ?>" class="h3a">提醒记录</a>
    </h3>
    <table id="table" border="1" bordercolor="#CCCCCC" cellpadding="0" cellspacing="0">
    	<tr>
        	<th><input type="checkbox" class="indel" value="del" /></th>
        	<th style="text-align: center;">编号</th>
            <th style="text-align: center;">客户姓名</th>
            <th style="text-align: center;">身份证号</th>
            <th style="text-align: center;">手机号</th>
            <th style="text-align: center;">QQ</th>
            <th style="text-align: center;">邮箱</th>
            <th style="text-align: center;">下次联系时间</th>
            <th style="text-align: center;">提醒起始时间</th>
            <th style="text-align: center;">提示内容</th>
            <th style="text-align: center;">操作时间</th>
            <th style="text-align: center;">操作</th>
        </tr>
    </table>
    <div id="page"><a href="javascript:;" class="selbox">全选</a><a href="javascript:;" class="anti">反选</a><a href="javascript:;" class="unselbox">全不选</a>&nbsp;&nbsp;对选中项进行&nbsp;&nbsp;<a href="javascript:;" id="dely">删除</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="withAdd('__APP__/With/withadd/Cid/<?php echo ($Cid); ?>')" >新增</a></div>
</div>
</body>
</html>