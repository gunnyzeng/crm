<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" class="bg-dark">
<head>
<meta charset="utf-8" />
<title>登录 | 客户管理系统</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="__ADMINCSS__/app.v2.css" type="text/css" />
<link rel="stylesheet" href="__ADMINCSS__/font.css" type="text/css" cache="false" />
<!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->

</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
  <div class="container aside-xxl" id="content" style="margin-top: 10%;"> 
    <a class="navbar-brand block" href="javascript:void(0);">客户管理系统</a>
    <section class="panel panel-default bg-white m-t-lg">
      <header class="panel-heading text-center"> </header>
      <form action="__APP__/Index/login" method="post" class="panel-body wrapper-lg">
        <div class="form-group">
     
          <input type="text" name="username" id="username" placeholder="用户名/邮箱/手机号" class="form-control input-lg">
        </div>
        <div class="form-group">
        
          <input type="password" name="password" id="password" placeholder="密码" class="form-control input-lg">
        </div>
         <div class="form-group">
          
          <input type="text" name="code" id="code" placeholder="验证码" class="form-control input-lg" style="width: 60%;float: left;">
           <img src="__APP__/Public/verify/" border="0" id="verify" style="float: right;" />
        </div>
         <br> <br>
        
       
      
        
        <div class="line line-dashed"></div>
          <button type="submit" class="btn btn-primary" id="submit" style="width: 100%">登录</button>
      </form>
      
 			<p class="text-center" style="color: red;display: none;font-size: 18px;" id="errortips">登录失败</p>
    
    </section>
  </div>
</section>
<!-- footer -->
<footer id="footer">
  <div class="text-center padder">
    <p style="color: white"> 武汉大学人力资源管理培训中心<br>
      &copy; 2017</p>
  </div>
</footer>
<!-- / footer --> <script src="__ADMINJS__/app.v2.js"></script> <!-- Bootstrap --> <!-- App -->
</body>

<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/Public.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
<script>
$(function() {
	$('#content input').eq(0).focus();
    $('body input:text, input:password, textarea').focus(function() {
		$(this).css({'border':'solid 1px #398700','boxShadow':'0px 0px 8px #398700'});
	});
    $('body input:text, input:password, textarea').blur(function() {
		$(this).css({'border':'solid 1px #ccc','boxShadow':'none'});
	});
	$('#submit').click(function(event) {
		event.preventDefault();
		var username=$('#username').val();
		var password=$('#password').val();
		var code=$('#code').val();
		if (!/^[a-zA-Z0-9_-]|[\u4e00-\u9fa5]{2,16}$/.test(username)) {
			$('#username').css("border-color","red");
			$('#username').focus();
			return;
		}
		if (password.length<6) {
			$('#password').css("border-color","red");
			$('#password').focus();
			return;
		}
		if (!/^[a-zA-Z0-9]{4}$/.test(code)) {
			$('#code').css("border-color","red");
			$('#code').focus();
			return;
		}
		wintq('正在登录，请稍后...',4,20000,0,'');
		$.ajax({
			url:'__APP__/Index/login',
			dataType:"json",
			type:'POST',
			cache:false,
			data:'username='+username+'&password='+password+'&code='+code,
			success: function(data) {
				if (data.s=='ok') {
					$("#errortips").css("display","none");
					window.location.href='__APP__/Index/main/';
					
				}else {
					$("#errortips").css("display","");
					rcode();
				}
			}
		});
	});
	//更换验证码
	function rcode(obj) {
		obj.attr('src','__APP__/Public/verify/'+Math.random());
	}
	$('#verify').click(function() {
		rcode($(this));
	});
});
</script>
</html>