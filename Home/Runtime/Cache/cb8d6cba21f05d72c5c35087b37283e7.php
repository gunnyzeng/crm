<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加提醒|<?php echo ($configcache['Title']); ?></title>
<link rel="stylesheet" type="text/css" href="__CSS__/content.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/public.css"  />
<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/Public.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
<script type="text/javascript" src="__JS__/My97DatePicker/WdatePicker.js"></script>
<script>
$(document).ready(function() {
	var $dldd=$('#dl dd');
	$('.button').click(function() {
		var 
			cid=$('#dl input:hidden').val(),					//客户编号
			nexttime=$dldd.find('.Wdate').eq(0).val(),			//开始时间
			remindtime=$dldd.find('.Wdate').eq(1).val(),		//结束时间
			description=$dldd.find('.textarea').val();			//描述
			status=$dldd.find('.status:checked').val();			//状态
		if (nexttime=='') {
			wintq('请选择下次联系日期',2,1000,0,'');
			return;
		}
		wintq('正在处理，请稍后...',4,20000,0,'');
		$.ajax({
			url:'__APP__/With/withadd_do/',
			dataType:'json',
			type:'POST',
			data:'cid='+cid+'&nexttime='+nexttime+'&remindtime='+remindtime+'&description='+description+'&status='+status,
			success: function(data) {
				if (data.s=='ok') {
					wintq('添加成功',1,1000,0,'__APP__/With/withadd?Cid=<?php echo ($result["ID"]); ?>');
				}else {
					wintq(data.s,3,1000,1,'');
				}
			}
		});
	});
});
</script>
</head>
<body>
<div id="content">
	<dl id="dl">
    	<input type="hidden" value="<?php echo ($result['ID']); ?>" name="Cid" />
        <dd>
        	<span class="dd_left" style="width: 100px;">客户姓名：</span>
        	<span class="dd_right"><?php echo ($result['Customername']); ?></span>
        </dd>
        <dd>
        	<span class="dd_left" style="width: 100px;">身份证号：</span>
            <span class="dd_right"><?php echo ($result['UserID']); ?></span>
        </dd>
        <dd>
            <span class="dd_left" style="width: 100px;">提示内容：</span>
            <span class="dd_right"><textarea name="description" class="textarea"></textarea></span>
        </dd>
        <dd>
        	<span class="dd_left" style="width: 120px;">提醒结束时间：</span>
        	<span class="dd_right">
            	<input id="d4312" class="Wdate" type="text"  onFocus="WdatePicker({minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2050-10-01',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
            	<font>*</font>
            </span>
        </dd>
        <dd>
        	<span class="dd_left" style="width: 120px;">提醒起始时间：</span>
        	<span class="dd_right">
            	<!--<input class="Wdate" name="Birthday" type="text" onfocus="WdatePicker({startDate:'%y-%M-01 00:00:00',dateFmt:'yyyy-MM-dd HH:mm:ss',alwaysUseStartDate:true})">-->
                
            	<input id="d4311" class="Wdate" type="text" value="<?php echo ($result['StartDate']); ?>" onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4312\')||\'2050-10-01\'}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
            	<font>*</font>
            </span>
        </dd>
        <dd>
        	<span class="dd_left" style="width: 100px;">提醒：</span>
        	<span class="dd_right">
            	<label><input type="radio" value="1" name="status" class="status"/> 关闭</label>
                <label><input type="radio" value="0" name="status" class="status" checked="checked"/> 提醒</label>
                <font>* 关闭后将不再提醒</font>
            </span>
        </dd>
        <dd><input type="button" class="button" value="提 交" /></dd>
    </dl>
</div>
</body>
</html>