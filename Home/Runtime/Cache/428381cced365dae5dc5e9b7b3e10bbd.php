<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加销售信息|<?php echo ($configcache['Title']); ?></title>
<link rel="stylesheet" type="text/css" href="__CSS__/content.css"  />
<link rel="stylesheet" type="text/css" href="__CSS__/public.css"  />
<script type="text/javascript" src="__JS__/jquery.js"></script>
<script type="text/javascript" src="__JS__/Public.js"></script>
<script type="text/javascript" src="__JS__/winpop.js"></script>
<script type="text/javascript" src="__JS__/check.js"></script>
<script type="text/javascript" src="__JS__/My97DatePicker/WdatePicker.js"></script>
<script>
$(document).ready(function() {
    var $client = $('#client');
    $('.submit').click(function() {
        wintq('正在处理，请稍后...',4,20000,0,'');
        $('form').submit();
    });
});

function calculate(){
   var products = $.find('.Products');
    var totalprice = 0;
   for(var index=0;index<products.length;index++){
      var e = products[index].name;
      totalprice += parseFloat($("#"+e).find("option:selected").attr("money"));
   }
    $("input[name='Price']").val(totalprice);
}
</script>
</head>
<body>

<div id="content" style="padding-bottom:20px;">
<div style="text-align: center;margin-top: -10px;"><a style="float: none;margin-left: 0px;"><font size="6px;">修改成交信息</font></a></div>
    <form action="__APP__/Client/contactedit_do" method="post">
     <table id="client" style="border: none;" bordercolor="#CCCCCC" cellpadding="0" cellspacing="0">
      <tr class="tr">
          <td class="left">姓名：</td>
          <td><a href="__APP__/Client/clientedit/id/<?php echo ($result['Cid']); ?>"><?php echo ($result['Customername']); ?></a></td>
            <td class="left">身份证号：</td>
          <td><?php echo ($result['UserID']); ?></td>
            <td class="left">学历：</td>
            <td><?php echo ($result['Education']); ?></td>
          
      </tr>
      <tr class="tr">
        <td class="left">手机号：</td>
        <td><?php echo ($result['Phone']); ?></td>
        <td class="left">QQ：</td>
        <td><?php echo ($result['Qq']); ?></td>
        <td class="left">听课证号：</td>
        <td><input name="PermitID" type="text" class="ctext" size="20" value="<?php echo ($result['PermitID']); ?>"/></td>
      </tr>
      <tr class="tr">
          <td class="left">业绩：</td>
          <td>
              <select name="Performance" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 92): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['Performance'] == $vo['MenuName']): ?>selected<?php endif; ?> ><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
            <td class="left">优惠：</td>
            <td>
              <select name="Discount" class="select">
                  <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 95): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['Discount'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
             <td class="left">支付方式：</td>
            <td>
                <select name="Payment" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 101): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['Payment'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
        </tr>
        <tr class="tr">
            <td class="left">考期安排：</td>
            <td>
                <select name="ExamTime" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 113): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['ExamTime'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
            <td class="left">考试状态：</td>
            <td>
                <select name="ExamStatus" class="select">
                    <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == 117): ?><option value="<?php echo ($vo["MenuName"]); ?>" <?php if($result['ExamStatus'] == $vo['MenuName']): ?>selected<?php endif; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
            <td class="left">成交日期：</td>
            <td>
                <input class="Wdate"  name="Saletdate" type="text" value="<?php echo ($result['Saletdate']); ?>" onfocus="WdatePicker({startDate:'%y-%M-01',dateFmt:'yyyy-MM-dd',alwaysUseStartDate:true})">
            </td>
        </tr>
       <?php if(is_array($products)): $prindex = 0; $__LIST__ = $products;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$pr): $mod = ($prindex % 2 );++$prindex; if($prindex%3 == 1): ?><tr class="tr"><?php endif; ?>
                <td class="left"><?php echo ($pr["MenuName"]); ?></td>
                 <td>
                  <select name="Products_<?php echo ($pr["ID"]); ?>" class="select Products" onblur="calculate()" id="Products_<?php echo ($pr["ID"]); ?>">
                     <option value="请选择" money="0" <?php if($result['ProductsDetail']['Products_$pr.ID'] == '请选择'): ?>selected<?php endif; ?>>请选择</option>
                  <?php if(is_array($volist)): $i = 0; $__LIST__ = $volist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo["Sid"] == $pr['ID']): ?><option value="<?php echo ($vo["MenuName"]); ?>" money="<?php echo ($vo["Description"]); ?>" <?php $pname = 'Products_'.$pr['ID']; $pval = $result['ProductsDetail'][$pname]; if($pval==$vo['MenuName']) echo "selected=\"selected\""; ?>><?php echo ($vo["MenuName"]); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                  </select>
                  </td>
             <?php if($prindex%3 == 0): ?></tr><?php endif; endforeach; endif; else: echo "" ;endif; ?>
         <?php if($productsCount%3 == 0): ?><tr class="tr">
                <td class="left">价格：</td>
                 <td><input name="Price" type="text" class="ctext" size="30" value="<?php echo ($result['Price']); ?>" /></td>
              </tr>
          </tr>
          <?php else: ?>
              <td class="left">价格：</td>
              <td><input name="Price" type="text" class="ctext" size="30"  value="<?php echo ($result['Price']); ?>"/></td><?php endif; ?>
      <tr class="tr">
          <td class="left">备注说明：</td>
          <td><textarea name="Content" class="textarea" style="width:200px; height:60px; margin:6px 0px;"><?php echo ($result['Content']); ?></textarea></td>
           <td colspan="4" style="border: solid 0px;"> 
           <input type="submit" class="submit" value="提交" style="margin:20px 0 0 0px;float: right;" />
          </td>
        </tr>
    </table>
   
    <input type="hidden" name="ID" value="<?php echo ($result['ID']); ?>" />
     <input type="hidden" name="Cid" value="<?php echo ($result['Cid']); ?>" />
   
    </form>
</div>
</body>
</html>