<?php
//联系人表
class ContactModel extends RelationModel {

	//自动完成
	protected $_auto = array ( 
		array('Dtime','Dtime',1,'callback'),
		array('FinalTime','Dtime',3,'callback'),
		array('Uid','Uid',1,'callback'),
	);
	//添加当前时间
	protected function Dtime() {
		return date('Y-m-d H:i:s');
	}
	//添加用户ID
	protected function Uid() {
		return $_SESSION['ThinkUser']['ID'];
	}
	//关联查询
	protected $_link = array(
		'Client' => array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'Client',
			'foreign_key'=>'Cid',
			'mapping_name'=>'Customername,UserID',
			'mapping_fields'=>'Customername,UserID,Phone,Qq,Education,Recycle',
			'as_fields'=>'Customername,UserID,Phone,Qq,Education,Recycle'
		),
	);
}
?>