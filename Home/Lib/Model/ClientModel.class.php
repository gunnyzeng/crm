<?php
//客户管理
class ClientModel extends RelationModel {

	//自动完成
	protected $_auto = array ( 
		array('FinalTime','Dtime',3,'callback'),
		array('Dtime','Dtime',1,'callback'),
		array('Uid','Uid',1,'callback'),
	);
	//添加当前时间
	protected function Dtime() {
		return date('Y-m-d H:i:s');
	}
	//添加用户ID
	protected function Uid() {
		return $_SESSION['ThinkUser']['ID'];
	}
	//关联查询
	protected $_link = array(
		'Contact' => array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'Contact',
			'foreign_key'=>'Cid',
			'mapping_name'=>'Contact',
			'mapping_fields'=>'ID,Cid,ContactName',
			'as_fields' => 'ContactName'
		),
		'User' => array(
			'mapping_type'=>BELONGS_TO,
			'class_name'=>'User',
			'foreign_key'=>'Uid',
			'mapping_name'=>'Username',
			'mapping_fields'=>'Username',
			'as_fields' => 'Username'
		),
	);
}
?>