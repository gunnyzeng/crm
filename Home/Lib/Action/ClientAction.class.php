<?php
//客户管理类
class ClientAction extends CommonAction {
	public function index() {
		parent::userauth2(65);
		$user=M("user");
		$volist=$user->where("Status=0")->getField("ID,Username");
		

		$this->assign("volist",$volist);
		$this->display();
	}
	//客户资料Ajax请求
	public function indexajax() {
		$keyword = I('param.keyword','','htmlspecialchars');
		$query = I('param.query','','htmlspecialchars');
		$querydate = I('param.querydate','','htmlspecialchars');
		$client = D('Client');
		import('ORG.Util.Page');						// 导入分页类
	

		$where = "c.Recycle= 0";

		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where .=" and c.Uid = ".$_SESSION['ThinkUser']['ID'];
		}
		if ($Cid!='' && is_numeric($Cid)) {
			$where .=" and  c.Cid  = $Cid";
		}
		//其他字段关键字模糊匹配
		if(!empty($keyword)){
			   $where .=" and (u.Username  like '%$keyword%' or u.Realname like '%$keyword%'";
			   $where .=" or c.Email  like '%$keyword%'";
			   $where .=" or c.UserID  like '%$keyword%'";
			   $where .=" or c.Customername  like '%$keyword%'";
			   $where .=" or c.From  like '%$keyword%'";
			   $where .=" or c.Phone  like '%$keyword%'";
			   $where .=" or c.CustomerLevel  like '%$keyword%'";
			   $where .=" or c.CompanyName  like '%$keyword%'";
			   $where .=" or c.Intro  like '%$keyword%'";
			   $where .=" or c.Education  like '%$keyword%'";
			   if(!empty(intval($keyword))){
			   	   $where .=" or c.FinalTime  like '%".$keyword."%'";
			   }
			   $where .=" or c.Qq  like '%$keyword%')";
		}
		
		if(!empty($query)){
			if(empty($querydate)){
				$querydate=date("Y-m-d");
			}
			$datearr = explode("-", $querydate);
			$year = $datearr[0];
			$month = $datearr[0]."-".$datearr[1];
			$day = $querydate;
			
			//日期条件
			if(stripos($query,"year")!==false){//年
				$where .=" and date_format(c.Dtime,'%Y')='$year'";
			}else if(stripos($query,"month")!==false){//月
				$where .=" and date_format(c.Dtime,'%Y-%m')='$month'";
			}else if(stripos($query,"day")!==false){//日
				$where .=" and date_format(c.Dtime,'%Y-%m-%d')='$day'";
			}
			//销售信息
			if(stripos($query,"fsale")!==false){
				$where .=" and c.Salecount=0";
			}else if(stripos($query,"tsale")!==false){
				$where .=" and c.Salecount>0";
			}
			//共享客户
			if(stripos($query,"fshare")!==false){
				$where .=" and c.OpenShare=0";
			}else if(stripos($query,"tshare")!==false){
				$where .=" and c.OpenShare=1";
			}
		}

		$model = new Model();
		$ct = $model->query("select count(*) as num from tp_client c left join tp_user u on c.Uid = u.ID where $where");
	

		$count = $ct[0]['num'];	 		//总记录数

		$Page = new Page($count,15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		
		$volist = $model->query("select c.*,u.Username  from tp_client c left join tp_user u on c.Uid = u.ID where $where order by c.FinalTime desc limit $Page->firstRow,$Page->listRows");


		
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
		
			$i = 1;
			foreach($volist as $vo) {
				
				//将搜索的标为红色
				$customername = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Customername']);		
				$email = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Email']);	
				$qq = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Qq']);	
				$phone = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Phone']);
				$userID = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['UserID']);
				$Username = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Username']);
				$vo['FinalTime'] = date("Y-m-d", strtotime($vo['FinalTime']));
				$FinalTime = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['FinalTime']);
				$Intro = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Intro']);
				$CompanyName = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['CompanyName']);
				if(!empty($vo['Userthumbpath'])){
					$thumb = "<a href='".C('WEBROOT')."/".$vo['Userthumbpath']."' target='_blank'>有</a>";
				}else{
					$thumb = "无";
				}

				if ($b=$i % 2 == 0) { 
					$tr2 = 'tr2';
				}else {
					$tr2 = '';
				}
				if ($vo['OpenShare']==0) {
					$OpenShare = '<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/no.png" border="0" title="点击可共享客户" alt="'.$vo['ID'].'" data-title="'.$vo['OpenShare'].'" class="openshare" />';
				}else {
					$OpenShare = '<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/yes.png" border="0" title="点击关闭共享" alt="'.$vo['ID'].'" data-title="'.$vo['OpenShare'].'" class="openshare" />';
				}
				if($vo['From']=='客户填写'||!empty($vo['OldUid'])){
					if($vo['From']=='客户填写'){
						$addClass="style='color:blue'";
					}
					if(!empty($vo['OldUid'])){
						$addClass="style='color:green'";
					}

					
				}else{
					$addClass='';
				}
				$html .= "
					<tr class='tr ".$tr2."' $addClass>
						<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>
						<td class='tc'><a href='".$vo['ID']."' class='edit'>".$customername."</a><img class='add' src='".C('TMPL_PARSE_STRING.__IMAGE__')."/icon.png' alt='".C('TMPL_PARSE_STRING.__APP__')."/Client/contactadd?Cid=".$vo['ID']."' border='0' title='新增销售信息' /></td>
						<td class='tc'><a href='".C('TMPL_PARSE_STRING.__APP__')."/Client/wincontact?Cid=".$vo['ID']."'>".$vo['Salecount']."</a></td>
						<td class='tc'>".$CompanyName."</td>
						<td class='tc'>".$vo['Post']."</td>
						<td class='tc'>".$phone."</td>
						<td class='tc'>".$vo['Content']."</td>
						<td class='tc'>".$thumb."</td>
						<td class='tc'>".$Intro."</td>
						<td class='tc'>".$Username."</td>
						<td class='tc'>".$FinalTime."</td>
						
						<td class='tc fixed_w'><a onclick=\"withAdd('".__APP__."/With/withadd/Cid/".$vo['ID']."')\"><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/add.png' border='0' title='增加提醒' /></a><a href='".$vo['ID']."' class='del'><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/delete.png' border='0' title='删除' /></a></td>
					</tr>
				";
				$i++;
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>');
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='12'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}

	//打开或关闭共享请求
	public function opensharedo() {
		parent::userauth(72);
		$id = I('get.id','');
		$openshare = I('get.openshare','');
		$openshare = intval($openshare);
		$id = intval($id);
		$client = M('client');
		if ($openshare==0) {
			$openshare = 1;
		}else {
			$openshare = 0;
		}
		if ($client->where("ID = $id")->save(array('OpenShare' => $openshare))) {
			parent::operating(__ACTION__,0,'共享客户编号为：'.$id.'的数据');
			R('Public/errjson',array('ok'));
		}else {
			parent::operating(__ACTION__,1,'共享失败：'.$client->getError());
			R('Public/errjson',array('共享操作失败'));
		}
	}
	//查看共享客户
	public function openshare() {
		//验证用户权限
		parent::userauth2(73);
		$user=M("user");
		$volist=$user->where("Status=0")->getField("ID,Username");
		$this->assign("volist",$volist);
		$this->display();
	}
	//导出模板
	public function exportExcel($expTitle,$expCellName,$expTableData){
		 ob_clean();
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
        $fileName =  $xlsTitle.date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
        vendor("PHPExcel.PHPExcel");
       
        $objPHPExcel = new PHPExcel();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        
        //$objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
       // $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  Export time:'.date('Y-m-d H:i:s'));  
        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'1', $expCellName[$i][1]); 
        } 
          // Miscellaneous glyphs, UTF-8   
        for($i=0;$i<$dataNum;$i++){
          for($j=0;$j<$cellNum;$j++){
            
            if(stripos($expTableData[$i][$expCellName[$j][0]],"http")!==false){
            	$objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+2), "点击下载".$expCellName[$j][1]);
            	$objPHPExcel->getActiveSheet(0)->getCell($cellName[$j].($i+2))->getHyperlink()->setUrl($expTableData[$i][$expCellName[$j][0]]);   
            }else{
            	$objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+2), is_numeric($expTableData[$i][$expCellName[$j][0]])?" ".$expTableData[$i][$expCellName[$j][0]]:$expTableData[$i][$expCellName[$j][0]]);
            }
          }             
        }  
        
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$fileName.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        $objWriter->save('php://output'); 
        exit;   
    }
    //导出客户信息
	public function exportClients(){
		parent::userauth2(87);
		
        $keyword = I('get.keyword','','htmlspecialchars');
		$query = I('get.query','','htmlspecialchars');
		$querydate = I('get.querydate','','htmlspecialchars');
		

		$where = "c.Recycle= 0";

		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where .=" and c.Uid = ".$_SESSION['ThinkUser']['ID'];
		}
	
		//其他字段关键字模糊匹配
		if(!empty($keyword)){
		
			   $where .=" and (u.Username  like '%$keyword%' or u.Realname like '%$keyword%'";
			   $where .=" or c.Email  like '%$keyword%'";
			   $where .=" or c.UserID  like '%$keyword%'";
			   $where .=" or c.Customername  like '%$keyword%'";
			   $where .=" or c.From  like '%$keyword%'";
			   $where .=" or c.Phone  like '%$keyword%'";
			   $where .=" or c.CustomerLevel  like '%$keyword%'";
			   $where .=" or c.Education  like '%$keyword%'";
			   $where .=" or c.CompanyName  like '%$keyword%'";
			   $where .=" or c.Intro  like '%$keyword%'";
			   if(!empty(intval($keyword))){
			   	   $where .=" or c.FinalTime  like '%".$keyword."%'";
			   }
			   $where .=" or c.Qq  like '%$keyword%')";
		}
		
		if(!empty($query)){
			if(empty($querydate)){
				$querydate=date("Y-m-d");
			}
			$datearr = explode("-", $querydate);
			$year = $datearr[0];
			$month = $datearr[0]."-".$datearr[1];
			$day = $querydate;
			
			//日期条件
			if(stripos($query,"year")!==false){//年
				$where .=" and date_format(c.FinalTime,'%Y')='$year'";
			}else if(stripos($query,"month")!==false){//月
				$where .=" and date_format(c.FinalTime,'%Y-%m')='$month'";
			}else if(stripos($query,"day")!==false){//日
				$where .=" and date_format(c.FinalTime,'%Y-%m-%d')='$day'";
			}
			//销售信息
			if(stripos($query,"fsale")!==false){
				$where .=" and c.Salecount=0";
			}else if(stripos($query,"tsale")!==false){
				$where .=" and c.Salecount>0";
			}
		}

		if (!empty(I('get.selectid',''))) {
			$selectid=explode(',',I('get.selectid',''));
			array_pop($selectid);
			$ids=join(',',$selectid);
			$where = " c.ID in ($ids) ";
		}
			

		$model = new Model();
		$xlsData = $model->query("select c.*,u.Username  from tp_client c left join tp_user u on c.Uid = u.ID where $where order by c.FinalTime desc");
		foreach ($xlsData as $k => $v)
        {

            $xlsData[$k]['Userthumbpath']=empty($xlsData[$k]['Userthumbpath'])?'':C('WEBROOT').$v['Userthumbpath'];
            $xlsData[$k]['Xuelithumbpath']=empty($xlsData[$k]['Xuelithumbpath'])?'':C('WEBROOT').$v['Xuelithumbpath'];
            $xlsData[$k]['Shenfenzthumbpath']=empty($xlsData[$k]['Shenfenzthumbpath'])?'':C('WEBROOT').$v['Shenfenzthumbpath'];
            $xlsData[$k]['Shenfenfthumbpath']=empty($xlsData[$k]['Shenfenfthumbpath'])?'':C('WEBROOT').$v['Shenfenfthumbpath'];
        }
		 $xlsName  = "客户信息";
        $xlsCell  = array(
	        array('ID','序列'),
	        array('Customername','姓名'),
	        array('CustomerLevel','客户级别'),
	        array('Salecount','成交量'),
	        array('UserID','身份证号'),
	        array('Sex','性别'),
	        array('Birthday','出生年月'),
	        array('Hobbies','兴趣'),
	        array('CompanyName','公司名称'),
	        array('CompanyAddress','公司地址'),
	        array('JobYears','工作年限'),
	        array('StartJobDate','开始工作时间'),
	        array('Post','职务'),
	        array('Peixun','是否培训'),
	        array('Phone','电话'),
	        array('OfficePhone','办公电话'),
	        array('ByPhone','备用电话'),
	        array('Qq','QQ'),
	        array('Email','邮箱'),
	        array('Address','家庭住址'),
	        array('School','毕业院校'),
	        array('Education','教育程度'),
	        array('Major','专业'),
	        array('Intro','介绍人'),
	        array('From','来源'),
	        array('Userthumbpath','个人照片'),
	        array('Xuelithumbpath','学历照片'),
	        array('Shenfenzthumbpath','身份证正面面照片'),
	        array('Shenfenfthumbpath','身份证反面照片'),
	        array('Content','备注'),
	        array('OpenShare','是否共享'),
	        array('Username','录入人'),
	        array('Uid','录入人编号'),
	        array('DTime','录入时间'),
	        array('FinalTime','最后更新时间')
        );
        $this->exportExcel($xlsName,$xlsCell,$xlsData);
	}
	 //导出销售信息
	public function exportContacts(){
		parent::userauth2(88);
		
        $keyword = I('get.keyword','','htmlspecialchars');
		$query = I('get.query','','htmlspecialchars');
		$querydate = I('get.querydate','','htmlspecialchars');
		

		$where = "c.Recycle= 0";
		if(!empty(I('get.Cid','','htmlspecialchars'))){
			$where = "c.Cid= ".I('get.Cid','','htmlspecialchars');
		}
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where .=" and c.Uid = ".$_SESSION['ThinkUser']['ID'];
		}
			 
		
	
		//其他字段关键字模糊匹配
		if(!empty($keyword)){
			   $where .=" and (u.Username  like '%$keyword%' or u.Realname like '%$keyword%'";
			   $where .=" or c.PermitID  like '%$keyword%'";
			   $where .=" or cl.UserID  like '%$keyword%'";
			    $where .=" or cl.Education  like '%$keyword%'";
			   $where .=" or cl.Customername  like '%$keyword%'";
			   $where .=" or c.ExamTime  like '%$keyword%'";
			   $where .=" or c.ExamStatus  like '%$keyword%'";
			   if(!empty(intval($keyword))){
			   	   $where .=" or c.FinalTime  like '%".$keyword."%'";
			   }
			   $where .=" or c.ProductsDetail  like '%$keyword%')";

		}
		
		if(!empty($query)){
			if(empty($querydate)){
				$querydate=date("Y-m-d");
			}
			$datearr = explode("-", $querydate);
			$year = $datearr[0];
			$month = $datearr[0]."-".$datearr[1];
			$day = $querydate;
			
			//日期条件
			if(stripos($query,"year")!==false){//年
				$where .=" and date_format(c.Saletdate,'%Y')='$year'";
			}else if(stripos($query,"month")!==false){//月
				$where .=" and date_format(c.Saletdate,'%Y-%m')='$month'";
			}else if(stripos($query,"day")!==false){//日
				$where .=" and date_format(c.Saletdate,'%Y-%m-%d')='$day'";
			}
		}

		if (!empty(I('get.selectid',''))) {
			$selectid=explode(',',I('get.selectid',''));
			array_pop($selectid);
			$ids=join(',',$selectid);
			$where = " c.ID in ($ids) ";
		}
			
		$xlsName  = "销售信息";
        $xlsCell  = array(
	        array('ID','序列'),
	        array('Customername','姓名'),
	        array('UserID','身份证号'),
	        array('PermitID','听课证号'),
	        array('Phone','手机号'),
	        array('Qq','QQ'),
	        array('Education','学历'),
	        array('Performance','业绩'),
	        array('Discount','优惠'),
	        array('Payment','支付方式'),
	        array('ExamTime','考试安排'),
	        array('ExamStatus','考试状态'),
	        array('Saletdate','销售时间'),
	        array('Price','价格'),
	        array('Userthumbpath','个人照片'),
	        array('Xuelithumbpath','学历照片'),
	        array('Shenfenzthumbpath','身份证正面面照片'),
	        array('Shenfenfthumbpath','身份证反面照片'),
        );

		$model = new Model();
		$xlsData = $model->query("select c.*,cl.Customername,cl.UserID,cl.Phone,cl.Qq,cl.Education,cl.Userthumbpath,cl.Xuelithumbpath,cl.Shenfenzthumbpath,cl.Shenfenfthumbpath,u.Username from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and c.Recycle= 0 order by c.Saletdate desc");
		
		foreach ($xlsData as $k => $v)
        {

            $xlsData[$k]['Userthumbpath']=empty($xlsData[$k]['Userthumbpath'])?'':C('WEBROOT').$v['Userthumbpath'];
            $xlsData[$k]['Xuelithumbpath']=empty($xlsData[$k]['Xuelithumbpath'])?'':C('WEBROOT').$v['Xuelithumbpath'];
            $xlsData[$k]['Shenfenzthumbpath']=empty($xlsData[$k]['Shenfenzthumbpath'])?'':C('WEBROOT').$v['Shenfenzthumbpath'];
            $xlsData[$k]['Shenfenfthumbpath']=empty($xlsData[$k]['Shenfenfthumbpath'])?'':C('WEBROOT').$v['Shenfenfthumbpath'];
        }
		//下拉菜单数据
		$dmenu = M('dmenu');
	
		$products = $dmenu->where("Sid = 0 and type='product'")->order('Sortid asc')->select();	
		foreach ($products as $index => $product) {
				$xlsCell[] = array('Products_'.$product['ID'],$product['MenuName']);
		}
		$xlsCell[] = array('Content','备注');
	    $xlsCell[] = array('Username','录入人');
	    $xlsCell[] = array('Uid','录入人编号');

		foreach ($xlsData as $key => $result) {
			$pro = explode("&&", $result['ProductsDetail']);
			foreach($pro as $value){ 
				$proname =  explode("=>", $value);
				$xlsData[$key][$proname[0]] = $proname[1];
			}
		}
	
        $this->exportExcel($xlsName,$xlsCell,$xlsData);
	}
	//共享数据请求
	public function openshareajax() {
		parent::userauth(73);

		$keyword = I('post.keyword','','htmlspecialchars');
		$query = I('post.query','','htmlspecialchars');
		$querydate = I('post.querydate','','htmlspecialchars');
		$client = D('Client');
		import('ORG.Util.Page');						// 导入分页类
	

		$where = "c.Recycle= 0 ";


		//其他字段关键字模糊匹配
		if(!empty($keyword)){
			   $where .=" and (u.Username  like '%$keyword%' or u.Realname like '%$keyword%'";
			   $where .=" or c.Email  like '%$keyword%'";
			   $where .=" or c.UserID  like '%$keyword%'";
			   $where .=" or c.Customername  like '%$keyword%'";
			   $where .=" or c.From  like '%$keyword%'";
			   $where .=" or c.Phone  like '%$keyword%'";
			   $where .=" or c.CustomerLevel  like '%$keyword%'";
			   $where .=" or c.CompanyName  like '%$keyword%'";
			   $where .=" or c.Intro  like '%$keyword%'";
			   $where .=" or c.Education  like '%$keyword%'";
			   if(!empty(intval($keyword))){
			   	   $where .=" or c.FinalTime  like '%".$keyword."%'";
			   }
			   $where .=" or c.Qq  like '%$keyword%')";
		}
		
		if(!empty($query)){
			if(empty($querydate)){
				$querydate=date("Y-m-d");
			}
			$datearr = explode("-", $querydate);
			$year = $datearr[0];
			$month = $datearr[0]."-".$datearr[1];
			$day = $querydate;
			
			//日期条件
			if(stripos($query,"year")!==false){//年
				$where .=" and date_format(c.Dtime,'%Y')='$year'";
			}else if(stripos($query,"month")!==false){//月
				$where .=" and date_format(c.Dtime,'%Y-%m')='$month'";
			}else if(stripos($query,"day")!==false){//日
				$where .=" and date_format(c.Dtime,'%Y-%m-%d')='$day'";
			}
			//销售信息
			if(stripos($query,"fsale")!==false){
				$where .=" and c.Salecount=0";
			}else if(stripos($query,"tsale")!==false){
				$where .=" and c.Salecount>0";
			}
			
			
		}
		//共享客户
			
		$where .=" and c.OpenShare=1";

		$model = new Model();
		$ct = $model->query("select count(*) as num from tp_client c left join tp_user u on c.Uid = u.ID where $where");
	

		$count = $ct[0]['num'];	 		//总记录数

		$Page = new Page($count,15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		
		$volist = $model->query("select c.*,u.Username  from tp_client c left join tp_user u on c.Uid = u.ID where $where order by c.FinalTime desc limit $Page->firstRow,$Page->listRows");
		
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
		
			$i = 1;
			foreach($volist as $vo) {
				
				//将搜索的标为红色
				$customername = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Customername']);		
				$email = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Email']);	
				$qq = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Qq']);	
				$phone = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Phone']);
				$userID = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['UserID']);
				$Username = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Username']);
				$vo['FinalTime'] = date("Y-m-d", strtotime($vo['FinalTime']));
				$FinalTime = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['FinalTime']);
				$Intro = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Intro']);
				$CompanyName = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['CompanyName']);
				if(!empty($vo['Userthumbpath'])||!empty($vo['Xuelithumbpath'])||!empty($vo['Shenfenzthumbpath'])||!empty($vo['Shenfenfthumbpath'])){
					$thumb = "有";
				}else{
					$thumb = "无";
				}

				if ($b=$i % 2 == 0) { 
					$tr2 = 'tr2';
				}else {
					$tr2 = '';
				}
				if ($vo['OpenShare']==0) {
					$OpenShare = '<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/no.png" border="0" title="点击可共享客户" alt="'.$vo['ID'].'" data-title="'.$vo['OpenShare'].'" class="openshare" />';
				}else {
					$OpenShare = '<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/yes.png" border="0" title="点击关闭共享" alt="'.$vo['ID'].'" data-title="'.$vo['OpenShare'].'" class="openshare" />';
				}
				if($vo['From']=='客户填写'){
					$addClass="style='color:blue'";
				}else{
					$addClass='';
				}
				$html .= "<tr class='tr ".$tr2."' $addClass>";
				if($_SESSION['ThinkUser']['Roleid']==1){
					$html .= "<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>";
				}
				
				$html .= "
						<td class='tc'><a href='".$vo['ID']."' class='edit'>".$customername."</a></td>
						<td class='tc'><a href='".C('TMPL_PARSE_STRING.__APP__')."/Client/openshare_contact_list?Cid=".$vo['ID']."'>".$vo['Salecount']."</a></td>
						<td class='tc'>".$CompanyName."</td>
						<td class='tc'>".$vo['Post']."</td>
						<td class='tc'>".$phone."</td>
						<td class='tc'>".$vo['Content']."</td>
						<td class='tc'>".$thumb."</td>
						<td class='tc'>".$Intro."</td>
						<td class='tc'>".$Username."</td>
						<td class='tc'>".$FinalTime."</td>
					</tr>
				";
				$i++;
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>');
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='12'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}
	//共享资料详细，基本资料
	public function openshare_company_detail() {
		//验证用户权限
		parent::win_userauth(73);
		$id = I('get.id','');
		if ($id=='' || !is_numeric($id)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->content='参数ID类型错误，请关闭本窗口';
			exit($this->display('Public:err'));
		}
		//下拉菜单数据
		$dmenu = M('dmenu');
		$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();
		//查出相应数据
		$client = D('Client');
		$result = $client->relation(true)->where("ID = $id AND Recycle = 0 AND OpenShare = 1")->find();
		if (!$result) {
			parent::operating(__ACTION__,1,'数据不存在');
			$this->content='不存在你要修改的数据，请关闭本窗口';
			exit($this->display('Public:err'));
		}
		$fileclass = D('Fileclass');
		$dir = $fileclass->where("FileName='图片' and Status='0'")->select();
		$this->assign('sid',$dir[0]['ID']);
		$this->assign('volist',$volist);
		$this->assign('result',$result);
		$this->display('opensharecompanydetail');
	}
	//共享资料详细，联系人资料
	public function openshare_contact_list() {
		//验证用户权限
		parent::userauth2(73);
		$Cid = I('get.Cid','');
		if ($Cid=='' || !is_numeric($Cid)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->error('参数不正确');
		}
		$this->assign('Cid',$Cid);
		$this->display('opensharecontactlist');
	}
	//共享客户联系信息ajax请求
	public function openshare_contact_ajax() {
		parent::userauth(73);
		$contact = D('Contact');
		import('ORG.Util.Page');						// 导入分页类
		
		$keyword = I('post.keyword','','htmlspecialchars');
		$query = I('post.query','','htmlspecialchars');
		$querydate = I('post.querydate','','htmlspecialchars');
		

		$where = "c.Recycle= 0";
		if(!empty(I('get.Cid','','htmlspecialchars'))){
			$where = "c.Cid= ".I('get.Cid','','htmlspecialchars');
		}
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where .=" and cl.Uid = ".$_SESSION['ThinkUser']['ID'];
		}
			 
		
	
		//其他字段关键字模糊匹配
		if(!empty($keyword)){
			   $where .=" and (u.Username  like '%$keyword%' or u.Realname like '%$keyword%'";
			   $where .=" or c.PermitID  like '%$keyword%'";
			   $where .=" or cl.UserID  like '%$keyword%'";
			   $where .=" or cl.Customername  like '%$keyword%'";
			   $where .=" or c.ExamTime  like '%$keyword%'";
			   $where .=" or c.ExamStatus  like '%$keyword%'";
			   if(!empty(intval($keyword))){
			   	   $where .=" or c.FinalTime  like '%".$keyword."%'";
			   }
			   $where .=" or c.ProductsDetail  like '%$keyword%')";

		}
		
		if(!empty($query)){
			if(empty($querydate)){
				$querydate=date("Y-m-d");
			}
			$datearr = explode("-", $querydate);
			$year = $datearr[0];
			$month = $datearr[0]."-".$datearr[1];
			$day = $querydate;
			
			//日期条件
			if(stripos($query,"year")!==false){//年
				$where .=" and date_format(c.Saletdate,'%Y')='$year'";
			}else if(stripos($query,"month")!==false){//月
				$where .=" and date_format(c.Saletdate,'%Y-%m')='$month'";
			}else if(stripos($query,"day")!==false){//日
				$where .=" and date_format(c.Saletdate,'%Y-%m-%d')='$day'";
			}
		}

		$Mt = new Model();
		
		$ct = $Mt->query("select count(*) as num from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and cl.Recycle= 0");
		
		$count = $ct[0]['num'];
		
		//$count = $contact->where($where)->count();	    //总记录数
		$Page = new Page($count,15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		$volist = $Mt->query("select c.*,cl.Customername,cl.UserID,cl.Education from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and cl.Recycle= 0 order by c.FinalTime desc limit $Page->firstRow,$Page->listRows");
		
		
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
		
			//输出数据
			$i=1;
			foreach($volist as $vo) {
				//判断该客户资料是否已经删除到回收站
				if ($vo['Recycle']==0) {
					//将搜索的标为红色
					$Customername = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Customername']);		
					$PermitID = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['PermitID']);	
					$UserID = str_replace(substr($vo['UserID'],6,8), "<span style='color:brown'>".substr($vo['UserID'],6,8)."</span>", $vo['UserID']);
					$UserID = str_replace($keyword,'<font>'.$keyword.'</font>',$UserID);
					$ExamTime = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['ExamTime']);	
					$ExamStatus = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['ExamStatus']);
					$productArr = explode("&&", $vo['ProductsDetail']);
					$ProductsDetail = "";
					foreach ($productArr as $value) {
						$arr = explode("=>", $value);

						if($arr[1]=="请选择"){
							continue;
						}
						$ProductsDetail .= str_replace($keyword,'<font>'.$keyword.'</font>',$arr[1])." ";
					}
					if ($b=$i % 2 == 0) { 
						$tr2 = 'tr2';
					}else {
						$tr2 = '';
					}
					$html .= "
						<tr class='tr ".$tr2."'>
							<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>
							<td class='tc'><a href='".$vo['ID']."' class='edit'>".$Customername."</td>
							<td class='tc'>".$UserID."</td>
							<td class='tc'>".$vo['Education']."</td>
							<td class='tc'>".$ExamTime."</td>
							<td class='tc'>".$ExamStatus."</td>
							<td class='tc'>".$ProductsDetail."</td>
							<td class='tc'>".$vo['Price']."</td>
							<td class='tc'>".$vo['Payment']."</td>
							<td class='tc'>".$vo['Saletdate']."</td>
							
						</tr>
					";
					$i++;
				}
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>');
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='11'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}
	//共享查看联系人的详细信息
	public function openshare_contact_detail() {
		//验证用户权限
		parent::win_userauth(73);
		$id = I('get.id','');
		if ($id=='' || !is_numeric($id)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->content='参数ID类型错误，请关闭本窗口';
			exit($this->display('Public:err'));
		}
		//下拉菜单数据
		$dmenu = M('dmenu');
		$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();
		//查出相应数据
		$contact = D('Contact');
		$result = $contact->relation(true)->where("ID = $id AND Recycle = 0")->find();
		if (!$result) {
			parent::operating(__ACTION__,1,'数据不存在');
			$this->content='不存在你要修改的数据，请关闭本窗口';
			exit($this->display('Public:err'));
		}

		$this->assign('volist',$volist);
		$this->assign('result',$result);
		

		$this->display('opensharecontactdetail');
	}
	//新增客户
	public function clientadd() {
		
		parent::win_userauth(66);
		$sid = 0;
		$type = explode('|',C('UPLOAD_FILE_PIC_TYPE'));
		$filetype='';
		foreach($type as $val) {
			$filetype .= '*.'.$val.";";
		}
		
		$dmenu=M('dmenu');
		$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();

		$fileclass = D('Fileclass');
		$dir = $fileclass->where("ClassName='图片' and Status='0' and Uid=".$_SESSION['ThinkUser']['ID'])->select();
	
		$this->assign('sid',$dir[0]['ID']);
		$this->assign('volist',$volist);
		$this->assign('filetype',$filetype);
		$this->display('clientadd');
	}

	//表单提交
	public function information(){
			$user = M('user');
			$u = $user->where("Status=0 and ID=".I('get.form',''))->select();
			if(count($u)!=1){
				parent::operating(__ACTION__,1,'参数错误');
				$this->content='链接有误，请关闭本窗口';
				exit($this->display('Public:err'));
			}

			$this->assign('Uid',I('get.form',''));	
			$sid = 0;
			$type = explode('|',C('UPLOAD_FILE_PIC_TYPE'));
			$filetype='';
			foreach($type as $val) {
				$filetype .= '*.'.$val.";";
			}
			
			$dmenu=M('dmenu');
			$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();

			$fileclass = D('Fileclass');
			$dir = $fileclass->where("ClassName='图片' and Status='0' and Uid=".$_SESSION['ThinkUser']['ID'])->select();
		

			$this->assign('sid',$dir[0]['ID']);
			$this->assign('volist',$volist);
			$this->assign('filetype',$filetype);
			$this->display('clientmobileadd');
			
		
	}
	//添加处理
	public function clientadd_do() {
		//验证用户权限
		if(I('post.From','','htmlspecialchars')!="客户填写" || empty(I('post.uid','','htmlspecialchars'))){
			parent::win_userauth(66);
		}
		
		if ($this->isPost()) {
			$data=array();
			$cont=array();
			$data['Customername'] = I('post.Customername','','htmlspecialchars');
			$data['UserID'] = I('post.UserID','','htmlspecialchars');
			$data['Sex'] = I('post.Sex','','htmlspecialchars');
			$data['Birthday'] = I('post.Birthday','','htmlspecialchars');
			$data['Hobbies'] = I('post.Hobbies','','htmlspecialchars');
			$data['CompanyName'] = I('post.CompanyName','','htmlspecialchars');
			$data['CompanyAddress'] = I('post.CompanyAddress','','htmlspecialchars');
			$data['JobYears'] = I('post.JobYears','','htmlspecialchars');
			$data['Post'] = I('post.Post','','htmlspecialchars');
			$data['Peixun'] = I('post.Peixun','','htmlspecialchars');
			$data['OpenShare'] = I('post.OpenShare','','htmlspecialchars');
			$data['Phone'] = I('post.Phone','','htmlspecialchars');
			$data['OfficePhone'] = I('post.OfficePhone','','htmlspecialchars');
			$data['StartJobDate'] = I('post.StartJobDate','','htmlspecialchars');
			$data['Education'] = I('post.Education','','htmlspecialchars');
			$data['ByPhone'] = I('post.ByPhone','','htmlspecialchars');
			$data['Qq'] = I('post.Qq','','htmlspecialchars');
			$data['Email'] = I('post.Email','','htmlspecialchars');
			$data['Address'] = I('post.Address','','htmlspecialchars');
			$data['School'] = I('post.School','','htmlspecialchars');
			$data['Major'] = I('post.Major','','htmlspecialchars');
			$data['CustomerLevel'] = I('post.CustomerLevel','','htmlspecialchars');
			$data['Intro'] = I('post.Intro','','htmlspecialchars');
			$data['From'] = I('post.From','','htmlspecialchars');
			$data['Userthumbpath'] = I('post.Userthumbpath','','htmlspecialchars');
			$data['Xuelithumbpath'] = I('post.Xuelithumbpath','','htmlspecialchars');
			$data['Shenfenzthumbpath'] = I('post.Shenfenzthumbpath','','htmlspecialchars');
			$data['Shenfenfthumbpath'] = I('post.Shenfenfthumbpath','','htmlspecialchars');
			$data['Content'] = I('post.Content','','htmlspecialchars');

			$where = array();
			
			if(!empty($data['UserID'])){
				 $where['UserID'] =  array('eq',$data['UserID']);
			}
			if(!empty($data['Email'])){
				 $where['Email'] =  array('eq',$data['Email']);
			}
			if(!empty($data['Qq'])){
				 $where['Qq'] =  array('eq',$data['Qq']);
			}
			if(!empty($data['Phone'])){
				 $where['Phone'] =  array('eq',$data['Phone']);
			}
			
           
			//自动完成验证与新增
			$client=D('Client');
			if(count($where)>0){
				$where['_logic']='or'; 
				$count = $client->where($where)->count();
				if($count>0){
					$this->error("身份证或手机号或邮箱或QQ,已存在！");
				}
			}
			if(empty($data['Customername'])){
				$this->error("客户姓名不能为空！");
			}
			
			if ($client->create($data)) {
				if(!empty(I('post.uid','','htmlspecialchars'))){
					$data['Uid'] = I('post.uid','','htmlspecialchars');
					$data['FinalTime'] = date('Y-m-d H:i:s');
					$data['DTime'] = date('Y-m-d H:i:s');
					$client->add($data);
					$this->success('添加成功',__APP__.'/Client/information/form/1',0);
					exit();
				}
				else{
					$client->add();
				}
				
				parent::operating(__ACTION__,0,'新增客户：'.$data['Username']);
				$this->success('添加成功',__APP__.'/Client/index',0);
			}else {
				parent::operating(__ACTION__,1,'新增失败：'.$client->getError());
				$this->error($client->getError());
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//修改客户资料
	public function clientedit() {
		parent::win_userauth(67);
		$id = I('get.id','');
		if ($id=='' || !is_numeric($id)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->content='参数ID类型错误，请关闭本窗口';
			exit($this->display('Public:err'));
		}
		
		$uid = $_SESSION['ThinkUser']['ID'];
		//下拉菜单数据
		$dmenu = M('dmenu');
		$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();
		//查出相应数据
		$client = D('Client');
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$result = $client->relation(true)->where("ID = $id AND Recycle = 0 AND Uid = $uid")->find();
		}else{
			$result = $client->relation(true)->where("ID = $id AND Recycle = 0")->find();
		}
		
		if (!$result) {
			parent::operating(__ACTION__,1,'没有找到数据：'.$id);
			$this->content='不存在你要修改的数据，请关闭本窗口';
			exit($this->display('Public:err'));
		}
		$fileclass = D('Fileclass');
		$dir = $fileclass->where("ClassName='图片' and Status='0'")->select();
		$this->assign('sid',$dir[0]['ID']);
		$this->assign('volist',$volist);
		$this->assign('result',$result);
		$this->display('clientedit');
	}
	//修改客户资料处理
	public function clientedit_do() {
		//验证用户权限
		parent::win_userauth(67);
		if ($this->isPost()) {
			$data=array();
			$data['ID'] = I('post.ID','','htmlspecialchars');
			$data['Customername'] = I('post.Customername','','htmlspecialchars');
			$data['UserID'] = I('post.UserID','','htmlspecialchars');
			$data['Sex'] = I('post.Sex','','htmlspecialchars');
			$data['Birthday'] = I('post.Birthday','','htmlspecialchars');
			$data['Hobbies'] = I('post.Hobbies','','htmlspecialchars');
			$data['CompanyName'] = I('post.CompanyName','','htmlspecialchars');
			$data['CompanyAddress'] = I('post.CompanyAddress','','htmlspecialchars');
			$data['JobYears'] = I('post.JobYears','','htmlspecialchars');
			$data['Post'] = I('post.Post','','htmlspecialchars');
			$data['Peixun'] = I('post.Peixun','','htmlspecialchars');
			$data['OpenShare'] = I('post.OpenShare','','htmlspecialchars');
			$data['Phone'] = I('post.Phone','','htmlspecialchars');
			$data['OfficePhone'] = I('post.OfficePhone','','htmlspecialchars');
			$data['StartJobDate'] = I('post.StartJobDate','','htmlspecialchars');
			$data['Education'] = I('post.Education','','htmlspecialchars');
			$data['ByPhone'] = I('post.ByPhone','','htmlspecialchars');
			$data['Qq'] = I('post.Qq','','htmlspecialchars');
			$data['Email'] = I('post.Email','','htmlspecialchars');
			$data['Address'] = I('post.Address','','htmlspecialchars');
			$data['School'] = I('post.School','','htmlspecialchars');
			$data['Major'] = I('post.Major','','htmlspecialchars');
			$data['CustomerLevel'] = I('post.CustomerLevel','','htmlspecialchars');
			$data['Intro'] = I('post.Intro','','htmlspecialchars');
			$data['From'] = I('post.From','','htmlspecialchars');
			$data['Userthumbpath'] = I('post.Userthumbpath','','htmlspecialchars');
			$data['Xuelithumbpath'] = I('post.Xuelithumbpath','','htmlspecialchars');
			$data['Shenfenzthumbpath'] = I('post.Shenfenzthumbpath','','htmlspecialchars');
			$data['Shenfenfthumbpath'] = I('post.Shenfenfthumbpath','','htmlspecialchars');
			$data['Content'] = I('post.Content','','htmlspecialchars');
			$data['FinalTime'] = date('Y-m-d H:i:s');
			$where = array();
			
			if(!empty($data['UserID'])){
				 $where['UserID'] =  array('eq',$data['UserID']);
			}
			if(!empty($data['Email'])){
				 $where['Email'] =  array('eq',$data['Email']);
			}
			if(!empty($data['Qq'])){
				 $where['Qq'] =  array('eq',$data['Qq']);
			}
			if(!empty($data['Phone'])){
				 $where['Phone'] =  array('eq',$data['Phone']);
			}
			

			$client = M('client');
			if(!empty($where)){
				$where['_logic']='or'; 
				$clients = $client->where($where)->select();

				if(count($clients)>1){
					$this->error("身份证或手机号或邮箱或QQ,已存在！");
				}else if(count($clients)==1 && $clients[0]['ID']!=$data['ID']){

					$this->error("身份证或手机号或邮箱或QQ,已存在！");
				}
			}
			
			if ($client->save($data)) {
				parent::operating(__ACTION__,0,'更新客户资料：'.$data['Username']);
				$this->success('客户资料更新成功',__APP__.'/Client/clientedit?id='.$data['ID']);
			}else {
				parent::operating(__ACTION__,1,'更新失败：'.$data['Username']);
				$this->error($client->getError());
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//删除客户资料到回收站
	public function client_del() {
		parent::userauth(68);
		//判断是否是ajax请求
		if ($this->isAjax()) {
			$id=I('post.id','');
			if ($id=='' || !is_numeric($id)) {
				parent::operating(__ACTION__,1,'参数错误');
				R('Public/errjson',array('参数ID类型错误'));
			}else {
				$id=intval($id);
				$client=M('client');
				$where=array('ID'=>$id);
				if ($client->where($where)->getField('ID')) {
					$contact=M('contact');
					$client->where($where)->save(array('Recycle' => 1));
					$contact->where("Cid=$id")->save(array('Recycle' => 1));
					parent::operating(__ACTION__,0,'删除成功：'.$id);
					R('Public/errjson',array('ok'));
				}else {
					parent::operating(__ACTION__,1,'删除失败：'.$this->getError());
					R('Public/errjson',array($this->getError()));
				}
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//批量删除客户资料到回收站
	public function client_indel() {
		//验证用户权限
		parent::userauth(68);
		if ($this->isAjax()) {
			if (!$delid=explode(',',I('post.delid',''))) {
				R('Public/errjson',array('请选中后再删除'));
			}
			//将最后一个元素弹出栈
			array_pop($delid);
			$id=join(',',$delid);
			$client=M('client');
			$contact=M('contact');
			$map['ID'] = array('in',$id);
			$co['Cid'] = array('in',$id);
			if ($client->where($map)->save(array('Recycle' => 1))) {
				$contact->where($co)->save(array('Recycle' => 1));
				parent::operating(__ACTION__,0,'删除成功：'.$delid);
				R('Public/errjson',array('ok'));
			}else {
				parent::operating(__ACTION__,1,'删除失败：'.$delid);
				R('Public/errjson',array('删除失败'));
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//批量转移客户
		public function client_translate() {
		//验证用户权限
		parent::userauth(89);
		if ($this->isAjax()) {
			if (!$selectid=explode(',',I('post.selectid',''))) {
				R('Public/errjson',array('请选中客户后再转移'));
			}
			$Uid = I('post.uid','');
			//将最后一个元素弹出栈
			array_pop($selectid);
			$id=join(',',$selectid);
			$client=M('client');
			$contact=M('contact');
			$map['ID'] = array('in',$id);
			$co['Cid'] = array('in',$id);

			if ($client->where($map)->save(array('OldUid'=>array('exp','Uid'),'Uid' => $Uid,'OpenShare'=>0,'FinalTime'=>date('Y-m-d H:i:s')))) {
				parent::operating(__ACTION__,0,'转移成功');
				R('Public/errjson',array('ok'));
			}else {
				parent::operating(__ACTION__,1,'转移失败');
				R('Public/errjson',array('转移失败'));
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	
	//联系人管理
	public function contact() {
		parent::userauth2(85);
		$this->display();
	}
	//生日提醒
	public function birthday() {
		parent::userauth2(86);
		$this->display();
	}
	//时间美化函数
	protected function Beautifytime($dtime) {
		$dtime = strtotime($dtime);
		$betime = time()-$dtime;
		$timename='';
		switch($betime) {
			case ($betime < 60):
				$timename = floor($betime).'秒前';
				break;
			case ($betime < 3600 && $betime > 60):
				$timename = floor(($betime/60)).'分钟前';
				break;
			case ($betime < 86400 && $betime > 3600):
				$timename = floor(($betime/60/60)).'小时前';
				break;
			case ($betime < 2592000 && $betime > 86400):
				$timename = floor(($betime/60/60/30)).'天前';
				break;
			case ($betime < 31536000 && $betime > 2592000):
				$timename = floor(($betime/60/60/30/12)).'个月前';
				break;
			case ($betime < 3153600000 && $betime > 31536000):
				$timename = floor(($betime/60/60/30/12/100)).'年前';
				break;
		}
		return $timename;
	}
	//生日提醒
	public function birthdayajax() {
		
		$birthday = I('get.birthday','','htmlspecialchars');
		$Cid = I('get.Cid','','htmlspecialchars');
		$contact = D('Client');
		import('ORG.Util.Page');						// 导入分页类
		$where=" contact.Recycle = 0 ";
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where.=" and (contact.OpenShare=1 or contact.Uid=".$_SESSION['ThinkUser']['ID'].") ";
		}
		
		$where.=" and contact.Birthday is not NULL and year(contact.Birthday)!=0 and (TO_DAYS(CONCAT(YEAR(NOW()),'-',MONTH(contact.Birthday),'-',DAY(contact.Birthday)))-TO_DAYS(NOW()) BETWEEN 0 and 7 ) ";
		
		
		
		$count = $contact->query("select count(*) from tp_client contact where (".$where.") or ()");
	
		//总记录数
		$Page = new Page($count[0]['num'],15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		
		$where = "select u.Username,contact.*,TO_DAYS(CONCAT(YEAR(NOW()),'-',MONTH(contact.Birthday),'-',DAY(contact.Birthday)))-TO_DAYS(NOW()) as days from tp_client contact left join tp_user u on contact.Uid=u.id  where ".$where." order by contact.FinalTime desc limit $Page->firstRow,$Page->listRows";
		$volist = $contact->query($where);
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
			//循环取出对应下拉菜单的数据
			for($i=0; $i<count($volist); $i++) {
				for($j=0; $j<count($dlist); $j++) {
					if ($volist[$i]['Post'] == $dlist[$j]['ID']) {
						$volist[$i]['Post'] = $dlist[$j]['MenuName'];
					}
				}
			}
			//输出数据
			$i=1;
			foreach($volist as $vo) {
				//判断该客户资料是否已经删除到回收站
				if ($vo['Recycle']==0) {
					
					//将搜索的标为红色
				$customername = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Customername']);		
				$email = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Email']);	
				$qq = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Qq']);	
				$phone = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Phone']);
				$userID = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['UserID']);
				$Username = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Username']);
				$vo['FinalTime'] = date("Y-m-d", strtotime($vo['FinalTime']));
				$FinalTime = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['FinalTime']);
				$Intro = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Intro']);
				$CompanyName = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['CompanyName']);
				if(!empty($vo['Userthumbpath'])||!empty($vo['Xuelithumbpath'])||!empty($vo['Shenfenzthumbpath'])||!empty($vo['Shenfenfthumbpath'])){
					$thumb = "有";
				}else{
					$thumb = "无";
				}
					$html .= "
					<tr class='tr ".$tr2."' $addClass>
						<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>
						<td class='tc'><a href='".$vo['ID']."' class='edit'>".$customername."</a><img class='add' src='".C('TMPL_PARSE_STRING.__IMAGE__')."/icon.png' alt='".C('TMPL_PARSE_STRING.__APP__')."/Client/contactadd?Cid=".$vo['ID']."' border='0' title='新增销售信息' /></td>
						<td class='tc'><a href='".C('TMPL_PARSE_STRING.__APP__')."/Client/wincontact?Cid=".$vo['ID']."'>".$vo['Salecount']."</a></td>
						<td class='tc'>".$CompanyName."</td>
						<td class='tc'>".$vo['Post']."</td>
						<td class='tc'>".$phone."</td>
						<td class='tc'>".$vo['Content']."</td>
						<td class='tc'>".$thumb."</td>
						<td class='tc'>".$Intro."</td>
						<td class='tc'>还有".$vo['days']."天过生日</td>
						<td class='tc'>".$Username."</td>
						<td class='tc'>".$FinalTime."</td>
						
						<td class='tc fixed_w'><a onclick=\"withAdd('".__APP__."/With/withadd/Cid/".$vo['ID']."')\"><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/add.png' border='0' title='增加提醒' /></a><a href='".$vo['ID']."' class='del'><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/delete.png' border='0' title='删除' /></a></td>
					</tr>
				";
					$i++;
				}
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>');
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='12'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}
	//客户资料Ajax请求
	public function contactajax() {
		
		$contact = D('Contact');
		import('ORG.Util.Page');						// 导入分页类
		
		$keyword = I('param.keyword','','htmlspecialchars');
		$query = I('param.query','','htmlspecialchars');
		$querydate = I('param.querydate','','htmlspecialchars');
		

		$where = "c.Recycle= 0";
		if(!empty(I('get.Cid','','htmlspecialchars'))){
			$where = "c.Cid= ".I('get.Cid','','htmlspecialchars');
		}
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where .=" and cl.Uid = ".$_SESSION['ThinkUser']['ID'];
		}
			 
		
	
		//其他字段关键字模糊匹配
		if(!empty($keyword)){
			   $where .=" and (u.Username  like '%$keyword%' or u.Realname like '%$keyword%'";
			   $where .=" or c.PermitID  like '%$keyword%'";
			   $where .=" or cl.UserID  like '%$keyword%'";
			   $where .=" or cl.Education  like '%$keyword%'";
			   $where .=" or cl.Customername  like '%$keyword%'";
			   $where .=" or c.ExamTime  like '%$keyword%'";
			   $where .=" or c.ExamStatus  like '%$keyword%'";
			   if(!empty(intval($keyword))){
			   	   $where .=" or c.FinalTime  like '%".$keyword."%'";
			   	   $where .=" or c.Saletdate  like '%".$keyword."%'";
			   }
 			
			   $where .=" or c.ProductsDetail  like '%$keyword%')";

		}
		
		if(!empty($query)){
			if(empty($querydate)){
				$querydate=date("Y-m-d");
			}
			$datearr = explode("-", $querydate);
			$year = $datearr[0];
			$month = $datearr[0]."-".$datearr[1];
			$day = $querydate;
			
			//日期条件
			if(stripos($query,"year")!==false){//年
				$where .=" and date_format(c.Saletdate,'%Y')='$year'";
			}else if(stripos($query,"month")!==false){//月
				$where .=" and date_format(c.Saletdate,'%Y-%m')='$month'";
			}else if(stripos($query,"day")!==false){//日
				$where .=" and date_format(c.Saletdate,'%Y-%m-%d')='$day'";
			}
		}

		$Mt = new Model();
		
		$ct = $Mt->query("select count(*) as num from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and cl.Recycle= 0");
		
		$count = $ct[0]['num'];
		
		//$count = $contact->where($where)->count();	    //总记录数
		$Page = new Page($count,15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		$volist = $Mt->query("select c.*,cl.Customername,cl.UserID,cl.Education from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and cl.Recycle= 0 order by c.FinalTime desc limit $Page->firstRow,$Page->listRows");
		
		$PriceTotal =  $Mt->query("select SUM(c.Price) as Prices from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and cl.Recycle= 0");
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
		
			//输出数据
			$i=1;
			foreach($volist as $vo) {
				//判断该客户资料是否已经删除到回收站
				if ($vo['Recycle']==0) {
					//将搜索的标为红色
					$Customername = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Customername']);	
					$Saletdate = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Saletdate']);		
					$PermitID = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['PermitID']);	
					$UserID = str_replace(substr($vo['UserID'],6,8), "<span style='color:brown'>".substr($vo['UserID'],6,8)."</span>", $vo['UserID']);
					$UserID = str_replace($keyword,'<font>'.$keyword.'</font>',$UserID);
					$Education = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['Education']);
					$ExamTime = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['ExamTime']);	
					$ExamStatus = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['ExamStatus']);
					$productArr = explode("&&", $vo['ProductsDetail']);

					$ProductsDetail = "";
					foreach ($productArr as $value) {
						$arr = explode("=>", $value);

						if($arr[1]=="请选择"){
							continue;
						}
						$ProductsDetail .= str_replace($keyword,'<font>'.$keyword.'</font>',$arr[1])." ";
					}
					if ($b=$i % 2 == 0) { 
						$tr2 = 'tr2';
					}else {
						$tr2 = '';
					}
					$html .= "
						<tr class='tr ".$tr2."'>
							<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>
							<td class='tc'><a href='".$vo['ID']."' class='edit'>".$Customername."</td>
							<td class='tc'>".$UserID."</td>
							<td class='tc'>".$Education."</td>
							<td class='tc'>".$ExamTime."</td>
							<td class='tc'>".$ExamStatus."</td>
							<td class='tc'>".$ProductsDetail."</td>
							<td class='tc'>".$vo['Price']."</td>
							<td class='tc'>".$vo['Payment']."</td>
							<td class='tc'>".$Saletdate."</td>
							<td class='tc fixed_w'><a href='".$vo['ID']."' class='del'><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/delete.png' border='0' title='删除' /></a></td>
						</tr>
					";
					$i++;
				}
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>','Price'=>$PriceTotal[0]['Prices']);
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='11'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}
	//窗口联系人管理
	public function wincontact() {
		parent::win_userauth(85);
		$Cid = I('get.Cid','');
		if ($Cid=='' || !is_numeric($Cid)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->error('参数不正确');
		}
		$this->assign('Cid',$Cid);
		$this->display();
	}
	//窗口客户资料Ajax请求
	public function wincontactajax() {
		$Cid = I('get.Cid','','htmlspecialchars');
		$contact = D('Contact');
		import('ORG.Util.Page');						// 导入分页类
		$where['Recycle'] = 0;
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where['Uid'] = $_SESSION['ThinkUser']['ID'];
		}
		if ($Cid!='' && is_numeric($Cid)) {
			$where['Cid']  = intval($Cid);
		}
		$count = $contact->where($where)->count();			//总记录数
		$Page = new Page($count,15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		$volist = $contact->relation(true)->where($where)->order('FinalTime desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$PriceTotal =  $Mt->query("select SUM(c.Price) as Prices from tp_contact c left join tp_client cl on c.Cid = cl.ID left join tp_user u on c.Uid=u.ID where $where and cl.Recycle= 0");
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
			//循环取出对应下拉菜单的数据
			for($i=0; $i<count($volist); $i++) {
				for($j=0; $j<count($dlist); $j++) {
					if ($volist[$i]['Post'] == $dlist[$j]['ID']) {
						$volist[$i]['Post'] = $dlist[$j]['MenuName'];
					}
				}
			}
			//输出数据
			$i=1;
			foreach($volist as $vo) {
				//判断该客户资料是否已经删除到回收站
				if ($vo['Recycle']==0) {
					//将搜索的标为红色
					if ($b=$i % 2 == 0) { 
						$tr2 = 'tr2';
					}else {
						$tr2 = '';
					}
					$html .= "
						<tr class='tr ".$tr2."'>
							<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>
							<td class='tc'>".$vo['ID']."</td>
							<td class='tc'>".$vo['Customername']."</td>
							<td class='tc'>".$vo['UserID']."</td>
							<td class='tc'>".$vo['PermitID']."</td>
							<td class='tc'>".$vo['ExamTime']."</td>
							<td class='tc'>".$vo['ExamStatus']."</td>
							<td class='tc'>".$vo['Price']."</td>
							<td class='tc'>".$vo['Payment']."</td>
							<td class='tc'>".$vo['Saletdate']."</td>
							<td class='tc'>".$vo['FinalTime']."</td>
							<td class='tc fixed_w'><a href='".$vo['ID']."' class='edit'><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/edit.png' border='0' title='查看详细/修改' /></a><a href='".$vo['ID']."' class='del'><img src='".C('TMPL_PARSE_STRING.__IMAGE__')."/delete.png' border='0' title='删除' /></a></td>
						</tr>
					";
					$i++;
				}
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>','Price'=>$PriceTotal[0]['Prices']);
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='9'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}
	//新增销售信息
	public function contactadd() {
		parent::win_userauth(69);
		$Cid = I('get.Cid','');
		if ($Cid=='' || !is_numeric($Cid)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->content='参数ID类型错误，请关闭本窗口';
			exit($this->display('Public:err'));
		}

		$where['Recycle'] = 0;
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where['Uid'] = $_SESSION['ThinkUser']['ID'];
		}
		if ($Cid!='' && is_numeric($Cid)) {
			$where['ID']  = intval($Cid);
		}

		//下拉菜单数据
		$dmenu = M('dmenu');
		$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();
		$products = $dmenu->where("Sid = 0 and Type='product'")->order('Sortid asc')->select();	
		$client = M('client');
		$result = $client->where($where)->find();
		$this->assign('volist',$volist);
		$this->assign('products',$products);
		$this->assign('productsCount',count($products));
		$this->assign('result',$result);
		$this->display('contactadd');
	}
	//新增销售信息
	public function contactadd_do() {
		parent::userauth2(69);
		if ($this->isPost()) {
			$cont=array();		
			$cont['Cid'] = I('post.Cid','','htmlspecialchars');	
			$cont['Performance'] = I('post.Performance','','htmlspecialchars');
			$cont['Discount'] = I('post.Discount','','htmlspecialchars');
			$cont['Payment'] = I('post.Payment','','htmlspecialchars');
			$cont['ExamTime'] = I('post.ExamTime','','htmlspecialchars');
			$cont['ExamStatus'] = I('post.ExamStatus','','htmlspecialchars');
			$cont['Saletdate'] = I('post.Saletdate','','htmlspecialchars');
			$cont['Price'] = I('post.Price','','htmlspecialchars');
			$cont['ProductsDetail'] = I('post.ProductsDetail','','htmlspecialchars');
			$cont['Content'] = I('post.Content','','htmlspecialchars');
			$cont['PermitID'] = I('post.PermitID','','htmlspecialchars');

			$dmenu = M('dmenu');
		    $products = $dmenu->where("Sid = 0 and Type='product'")->order('Sortid asc')->select();

		    foreach($products as $arr){ 
		    	$pname = "post.Products_".$arr['ID'];
		    	$ProductsDetail .="Products_".$arr['ID']."=>".I($pname ,'','htmlspecialchars')."&&";
		    }
		    $cont['ProductsDetail'] = $ProductsDetail;
		
			//自动完成验证与新增
			$contact=D('Contact');
			if ($contact->create($cont)) {
				$contact->add();
				$client = M('Client');
				$client->where("ID=".$cont['Cid'])->setInc('Salecount');
				parent::operating(__ACTION__,0,'新增成功：');
				$this->success('添加成功',__APP__.'/Client/index',0);
			}else {
				parent::operating(__ACTION__,1,'新增失败：'.$contact->getError());
				$this->error($contact->getError());
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//修改销售信息
	public function contactedit() {

		parent::win_userauth(70);
		$id = I('get.id','');
		if ($id=='' || !is_numeric($id)) {
			parent::operating(__ACTION__,1,'参数错误');
			$this->content='参数ID类型错误，请关闭本窗口';
			exit($this->display('Public:err'));
		}
		$uid = $_SESSION['ThinkUser']['ID'];
		//下拉菜单数据
		$dmenu = M('dmenu');
		$volist=$dmenu->where('Sid <> 0')->order('Sortid asc')->select();
		$products = $dmenu->where("Sid = 0 and Type='product'")->order('Sortid asc')->select();	
		//查出相应数据
		$contact = D('Contact');
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$tempresult = $contact->query("select c.*,cl.Customername,cl.UserID,cl.Phone,cl.Qq,cl.Education from tp_contact c left join tp_client cl on c.Cid=cl.ID where c.ID = $id AND c.Recycle = 0 AND cl.Uid = $uid");
			$result = $tempresult[0];

		}else{
			$result = $contact->relation(true)->where("ID = $id AND Recycle = 0")->find();
		}
		$pro = explode("&&", $result['ProductsDetail']);

		$result['ProductsDetail'] = array();
		foreach($pro as $value){ 
			$proname =  explode("=>", $value);
			$result['ProductsDetail'][$proname[0]] = $proname[1];
			
		}
		
     
		if (!$result) {
			parent::operating(__ACTION__,1,'没有找到数据：'.$id);
			$this->content='不存在你要修改的数据，请关闭本窗口';
			exit($this->display('Public:err'));
		}

		$this->assign('volist',$volist);
		$this->assign('result',$result);
		$this->assign('products',$products);
		$this->assign('productsCount',count($products));
		$this->display('contactedit');
	}
	//修改联系人信息处理
	public function contactedit_do() {
		parent::userauth2(70);
		if ($this->isPost()) {
			$cont=array();		
			$cont['Cid'] = I('post.Cid','','htmlspecialchars');	
			$cont['Performance'] = I('post.Performance','','htmlspecialchars');
			$cont['Discount'] = I('post.Discount','','htmlspecialchars');
			$cont['Payment'] = I('post.Payment','','htmlspecialchars');
			$cont['ExamTime'] = I('post.ExamTime','','htmlspecialchars');
			$cont['ExamStatus'] = I('post.ExamStatus','','htmlspecialchars');
			$cont['Saletdate'] = I('post.Saletdate','','htmlspecialchars');
			$cont['Price'] = I('post.Price','','htmlspecialchars');
			$cont['ProductsDetail'] = I('post.ProductsDetail','','htmlspecialchars');
			$cont['Content'] = I('post.Content','','htmlspecialchars');
			$cont['PermitID'] = I('post.PermitID','','htmlspecialchars');
			$cont['ID'] = I('post.ID','','htmlspecialchars');
			$cont['FinalTime'] = date('Y-m-d H:i:s');
			$dmenu = M('dmenu');
		    $products = $dmenu->where("Sid = 0 and Type='product'")->order('Sortid asc')->select();

		    foreach($products as $arr){ 
		    	$pname = "post.Products_".$arr['ID'];
		    	$ProductsDetail .="Products_".$arr['ID']."=>".I($pname ,'','htmlspecialchars')."&&";
		    }
		    $cont['ProductsDetail'] = $ProductsDetail;
		   
			//自动完成验证与新增
			$contact=D('Contact');
			if ($contact->save($cont)) {
				
				parent::operating(__ACTION__,0,'更新成功：');
				$this->success('修改成功',__APP__.'/Client/contactedit?id='.$cont['ID'],3);
			}else {
				parent::operating(__ACTION__,1,'更新失败：'.$contact->getError());
				$this->error($contact->getError());
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//删除联系人到
	public function contact_del() {
		parent::userauth(71);
		//判断是否是ajax请求
		if ($this->isAjax()) {
			$id=I('post.id','');
			if ($id=='' || !is_numeric($id)) {
				parent::operating(__ACTION__,1,'参数错误：'.$id);
				R('Public/errjson',array('参数ID类型错误'));
			}else {
				$id=intval($id);
				$client = M('client');
				
				$contact=M('contact');
				$where=array('ID'=>$id);
				if ($cont = $contact->where($where)->select()) {

					$client = M('Client');
					$client->where("ID=".$cont[0]['Cid'])->setDec('Salecount');

					$contact->where($where)->delete();
					parent::operating(__ACTION__,0,'删除成功：'.$id);
					R('Public/errjson',array('ok'));
				}else {
					parent::operating(__ACTION__,1,'删除失败：'.$contact->getError());
					R('Public/errjson',array($contact->getError()));
				}
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//批量删除联系人
	public function contact_indel() {
		//验证用户权限
		parent::userauth(71);
		if ($this->isAjax()) {
			if (!$delid=explode(',',I('post.delid',''))) {
				R('Public/errjson',array('请选中后再删除'));
			}
			//将最后一个元素弹出栈
			array_pop($delid);
			$client = M('client');
			$contact=M('contact');
			foreach($delid as $val) {
				$where=array('ID'=>$val);
				$cont=$contact->where($where)->select();
				$client->where("ID=".$cont[0]['Cid'])->setDec('Salecount');
				$contact->where($where)->delete();
				
			}
			parent::operating(__ACTION__,0,'删除成功：'.join(',',$delid));
			R('Public/errjson',array('ok'));
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//回收站
	public function recycle() {
		parent::userauth2(74);
		$this->display();
	}
	//已经删除数据请求
	public function recycleajax() {
		$keyword = I('get.keyword','','htmlspecialchars');
		$client = D('Client');
		import('ORG.Util.Page');						// 导入分页类
		$where['Recycle'] = 1;
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where['Uid'] = $_SESSION['ThinkUser']['ID'];
		}
		
		$where['CompanyName'] = $keyword;
		$count = $client->where($where)->count();			//总记录数
		$Page = new Page($count,15);					//实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header','条记录');
		$Page->setConfig('prev','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/prev.gif" border="0" title="上一页" />');
		$Page->setConfig('next','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/next.gif" border="0" title="下一页" />');
		$Page->setConfig('first','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/first.gif" border="0" title="第一页" />');
		$Page->setConfig('last','<img src="'.C('TMPL_PARSE_STRING.__IMAGE__').'/last.gif" border="0" title="最后一页" />');
		$show = $Page->show();							//分页显示输出
		$dmenu = M('dmenu');
		$dlist = $dmenu->order('Sortid asc')->select();
		$volist = $client->relation(true)->where($where)->order('FinalTime desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$html = '';
		//判断有无数据
		if (count($volist) > 0) {
			//循环取出对应下拉菜单的数据
			for($i=0; $i<count($volist); $i++) {
				for($j=0; $j<count($dlist); $j++) {
					if ($volist[$i]['ClientType'] == $dlist[$j]['ID']) {
						$volist[$i]['ClientType'] = $dlist[$j]['MenuName'];
					}
					if ($volist[$i]['Industry'] == $dlist[$j]['ID']) {
						$volist[$i]['Industry'] = $dlist[$j]['MenuName'];
					}
					if ($volist[$i]['ClientLevel'] == $dlist[$j]['ID']) {
						$volist[$i]['ClientLevel'] = $dlist[$j]['MenuName'];
					}
					if ($volist[$i]['ClientSource'] == $dlist[$j]['ID']) {
						$volist[$i]['ClientSource'] = $dlist[$j]['MenuName'];
					}
					if ($volist[$i]['FollowUp'] == $dlist[$j]['ID']) {
						$volist[$i]['FollowUp'] = $dlist[$j]['MenuName'];
					}
					if ($volist[$i]['Intent'] == $dlist[$j]['ID']) {
						$volist[$i]['Intent'] = $dlist[$j]['MenuName'];
					}
				}
			}
			//输出数据
			$i = 1;
			foreach($volist as $vo) {
				//将搜索的标为红色
				$company = str_replace($keyword,'<font>'.$keyword.'</font>',$vo['CompanyName']);		//公司名称
				if ($b=$i % 2 == 0) { 
					$tr2 = 'tr2';
				}else {
					$tr2 = '';
				}
				if(!empty($vo['Userthumbpath'])||!empty($vo['Xuelithumbpath'])||!empty($vo['Shenfenzthumbpath'])||!empty($vo['Shenfenfthumbpath'])){
					$thumb = "有";
				}else{
					$thumb = "无";
				}
				$html .= "
					<tr class='tr ".$tr2."'>
						<td class='tc'><input type='checkbox' class='delid' value='".$vo['ID']."' /></td>
						<td class='tc'>".$vo['Customername']."</td>
						<td class='tc'>".$vo['Salecount']."</td>
						<td class='tc'>".$vo['CompanyName']."</td>
						<td class='tc'>".$vo['Post']."</td>
						<td class='tc'>".$vo['Phone']."</td>
						<td class='tc'>".$vo['Content']."</td>
						<td class='tc'>".$thumb."</td>
						<td class='tc'>".$vo['Intro']."</td>
						<td class='tc'>".$vo['Username']."</td>
						<td class='tc'>".$vo['FinalTime']."</td>
						<td class='tc fixed_w'><a href='".$vo['ID']."' class='recy'>还原</a></td>
					</tr>
				";
				$i++;
			}
			$data = array('s'=>'ok','html'=>$html,'page'=>'<span class="page">'.$show.'</span>');
			echo json_encode($data);
		}else {
			$html = "<tr class='tr'><td class='tc' colspan='13'>暂无数据，等待添加～！</td></tr>";
			$data = array('s'=>'no','html'=>$html);
			echo json_encode($data);
		}
	}
	//还原操作
	public function reduction() {
		parent::userauth(75);
		//判断是否是ajax请求
		if ($this->isAjax()) {
			$id=I('post.id','');
			if ($id=='' || !is_numeric($id)) {
				parent::operating(__ACTION__,1,'参数错误');
				R('Public/errjson',array('参数ID类型错误'));
			}else {
				$id=intval($id);
				$client = M('client');
				$contact=M('contact');
				$where=array('ID'=>$id);
				if ($client->where($where)->getField('ID')) {
					$client->where($where)->save(array('Recycle' => 0));
					$contact->where("Cid=$id")->save(array('Recycle' => 0));
					parent::operating(__ACTION__,0,'还原成功');
					R('Public/errjson',array('ok'));
				}else {
					parent::operating(__ACTION__,1,'还原失败');
					R('Public/errjson',array('数据不存在'));
				}
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//批量还原
	public function reduction_in() {
		//验证用户权限
		parent::userauth(75);
		if ($this->isAjax()) {
			if (!$inid=explode(',',I('post.inid',''))) {
				R('Public/errjson',array('请选中后再删除'));
			}
			//将最后一个元素弹出栈
			array_pop($inid);
			$client = M('client');
			$contact=M('contact');
			foreach($inid as $val) {
				$where=array('ID'=>$val);
				$client->where($where)->save(array('Recycle' => 0));
				$contact->where("Cid=$val")->save(array('Recycle' => 0));
			}
			parent::operating(__ACTION__,0,'还原成功');
			R('Public/errjson',array('ok'));
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			$this->error('非法请求');
		}
	}
	//批量删除
	public function client_c_indel() {
		//验证用户权限
		parent::userauth(76);
		if ($this->isAjax()) {
			if (!$delid=explode(',',I('post.delid',''))) {
				R('Public/errjson',array('请选中后再删除'));
			}
			//将最后一个元素弹出栈
			array_pop($delid);
			$id=join(',',$delid);
			$client=M('client');
			$contact=M('contact');
			$co['Cid'] = array('in',$id);
			if ($client->delete("$id")) {
				$contact->where($co)->delete();
				parent::operating(__ACTION__,0,'删除成功');
				R('Public/errjson',array('ok'));
			}else {
				parent::operating(__ACTION__,1,'删除失败');
				R('Public/errjson',array('删除失败'));
			}
		}else {
			parent::operating(__ACTION__,1,'非法请求');
			R('Public/errjson',array('非法请求'));
		}
	}
}
?>