<?php
class PublicAction extends Action {
	public function verify() {
		ob_clean();
		import('ORG.Util.Image');
		Image::buildImageVerify('4','3','png','84','38','verify');
	}
	public function err($content) {
		$this->display();
	}
	public function errjson($content) {
		$err=array('s'=>$content);
		exit(json_encode($err));
	}
	public function sha1pow($pow) {
		return sha1(md5($pow));
	}
	public function location($title,$url) {
		header('Content-Type: text/html; charset=utf-8');	//输出头，防止中文乱码
		echo '<script>alert("'.$title.'"); window.top.location="'.$url.'"</script>';
		exit;
	}
	//跟单提醒
	public function remind() {
		$with = D('With');
		$Uid = $_SESSION['ThinkUser']['ID'];
		$date = date('Y-m-d H:i:s');
		$withlist = 0;
		$withcomplete = 0;
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$withlist = $with->relation(true)->where("Uid = $Uid AND Status = 0 AND RemindTime <= '$date'")->select();
		}else{
			$withlist = $with->relation(true)->where("Status = 0 AND RemindTime <= '$date'")->select();
		}
		
		$where="Recycle = 0 ";
		$where.=" and Birthday is not NULL and year(Birthday)!=0 and (TO_DAYS(CONCAT(YEAR(NOW()),'-',MONTH(Birthday),'-',DAY(Birthday)))-TO_DAYS(NOW()) BETWEEN 0 and 7 ) ";
		if($_SESSION['ThinkUser']['Roleid']!=1){
			$where.=" and Uid=".$_SESSION['ThinkUser']['ID'];
		}
		$client = D('Client');
		$count = $client->where($where)->count();
		foreach ($withlist as $key => $value) {
			$cl = $client->where("ID = ".$value['Cid'])->select();
			$withlist[$key]['Customername'] = $cl[0]['Customername'];
		}

		
		$this->assign('withcount',count($withlist));
		$this->assign('withlist',$withlist);
		$this->assign('birthdaycount',$count);
		$this->display('Public/info');
	}
	//时间美化函数
	public function Beautifytime($dtime) {
		$dtime = strtotime($dtime);
		$betime = time()-$dtime;
		$timename='';
		switch($betime) {
			case ($betime < 60):
				$timename = floor($betime).'秒前';
				break;
			case ($betime < 3600 && $betime > 60):
				$timename = floor(($betime/60)).'分钟前';
				break;
			case ($betime < 86400 && $betime > 3600):
				$timename = floor(($betime/60/60)).'小时前';
				break;
			case ($betime < 2592000 && $betime > 86400):
				$timename = floor(($betime/60/60/30)).'天前';
				break;
			case ($betime < 31536000 && $betime > 2592000):
				$timename = floor(($betime/60/60/30/12)).'个月前';
				break;
			case ($betime < 3153600000 && $betime > 31536000):
				$timename = floor(($betime/60/60/30/12/100)).'年前';
				break;
		}
		return $timename;
	}
}
?>